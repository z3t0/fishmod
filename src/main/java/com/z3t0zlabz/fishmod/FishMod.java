package com.z3t0zlabz.fishmod;

//import com.z3t0zlabz.fishmod.blocks.Furnace.AmmoniteFurnaceClass;
import com.z3t0zlabz.fishmod.blocks.Ore.*;
import com.z3t0zlabz.fishmod.blocks.decoration.*;
import com.z3t0zlabz.fishmod.entity.entityclass.CodFish;
import com.z3t0zlabz.fishmod.entity.EntityHandler;
import com.z3t0zlabz.fishmod.entity.entityclass.SalmonFish;
import com.z3t0zlabz.fishmod.entity.entityclass.TunaFish;
import com.z3t0zlabz.fishmod.items.*;
import com.z3t0zlabz.fishmod.items.food.*;
import com.z3t0zlabz.fishmod.items.rods.WoodFishingRodClass;
import com.z3t0zlabz.fishmod.items.tools.*;
import com.z3t0zlabz.fishmod.models.armor.*;

//import com.z3t0zlabz.fishmod.worldgen.SampleGenerationClass;
//import com.z3t0zlabz.fishmod.worldgen.WorldGenMyOres;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;

import com.z3t0zlabz.fishmod.lib.ProxyCommon;
import com.z3t0zlabz.fishmod.lib.References;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = References.MODID, version = References.VERSION)
public class FishMod
{
	@SidedProxy(clientSide = References.Client, serverSide = References.Common)
	public static ProxyCommon proxy;
	/* PUT ALL PUBLIC HERE */

	//public static  Mod.Instance instance;
	//Water Ores
	//Ocean
	public static Block AmmoniteOre ; //Ammonite Ore
	public static Block AquamarineOre;
	public static Block DiopsideOre;
	public static Block FluoriteOre;	
	public static Block TrilobiteOre;
	public static Block TurquoiseOre;
    //River
	public static Block BerylOre;
	public static Block GarnetOre;
	public static Block JasperOre;
	public static Block OpalOre;
	//Lake
	public static Block HematiteOre;
	public static Block PyriteOre;
	//Creative Tab
	public static CreativeTabs fishmod;

	//Tool Materials
	//Normal Tools
	//	public static final Item.ToolMaterial Example = EnumHelper.addToolMaterial(name, harvestLevel, maxUses, efficiency, damage, enchantability)
	public static final Item.ToolMaterial Ammonite = EnumHelper.addToolMaterial("AmmoniteMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Aquamarine = EnumHelper.addToolMaterial("AquamarineMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Diopside = EnumHelper.addToolMaterial("DiopsideMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Fluorite = EnumHelper.addToolMaterial("FluoriteMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Trilobite = EnumHelper.addToolMaterial("TrilobiteMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Turquoise = EnumHelper.addToolMaterial("TurquoiseMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Shark = EnumHelper.addToolMaterial("SharkMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Beryl = EnumHelper.addToolMaterial("BerylMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Garnet = EnumHelper.addToolMaterial("GarnetMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Jasper = EnumHelper.addToolMaterial("JasperMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial Opal = EnumHelper.addToolMaterial("OpalMaterial",2,192,5.0F,1.5F,12);
	public static final Item.ToolMaterial Hematite = EnumHelper.addToolMaterial("HematiteMaterial",2,192,5.0F,1.5F,12);
	public static final Item.ToolMaterial Pyrite = EnumHelper.addToolMaterial("PyriteMaterial",2,192,5.0F,1.5F,12);
	public static final Item.ToolMaterial Coral = EnumHelper.addToolMaterial("CoralMaterial",2,192,5.0F,1.5F,12); // We need to set values

	//Pickaxes
	public static final Item.ToolMaterial AmmonitePickaxeMaterial = EnumHelper.addToolMaterial("AmmonitePickaxeMaterial", 4, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial AquamarinePickaxeMaterial = EnumHelper.addToolMaterial("AquamarinePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial DiopsidePickaxeMaterial = EnumHelper.addToolMaterial("DiopsidePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial FluoritePickaxeMaterial = EnumHelper.addToolMaterial("FluoritePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial TrilobitePickaxeMaterial = EnumHelper.addToolMaterial("TrilobitePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial TurquoisePickaxeMaterial = EnumHelper.addToolMaterial("TurquoisePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial SharkPickaxeMaterial = EnumHelper.addToolMaterial("SharkPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial BerylPickaxeMaterial = EnumHelper.addToolMaterial("BerylPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial GarnetPickaxeMaterial = EnumHelper.addToolMaterial("GarnetPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial JasperPickaxeMaterial = EnumHelper.addToolMaterial("JasperPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial OpalPickaxeMaterial = EnumHelper.addToolMaterial("OpalPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial HematitePickaxeMaterial = EnumHelper.addToolMaterial("HematitePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial PyritePickaxeMaterial = EnumHelper.addToolMaterial("PyritePickaxeMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial CoralPickaxeMaterial = EnumHelper.addToolMaterial("CoralPickaxeMaterial", 2, 192, 5.0F, 1.5F, 12); // We need to set values


	//Sword Materials
	//	public static final Item.ToolMaterial Example = EnumHelper.addToolMaterial(name, harvestLevel, maxUses, efficiency, damage, enchantability)
	public static final Item.ToolMaterial AmmoniteSwordMaterial = EnumHelper.addToolMaterial("AmmoniteSwordMaterial", 0, 1100, 0, 9F, 12);
	public static final Item.ToolMaterial AquamarineSwordMaterial = EnumHelper.addToolMaterial("AquamarineSwordMaterial", 0, 500, 0, 7F, 12);
	public static final Item.ToolMaterial DiopsideSwordMaterial = EnumHelper.addToolMaterial("DiopsideSwordMaterial", 0, 1550, 0, 11F, 12);
	public static final Item.ToolMaterial FluoriteSwordMaterial = EnumHelper.addToolMaterial("FluoriteSwordMaterial", 0, 360, 0, 6F, 12);
	public static final Item.ToolMaterial TrilobiteSwordMaterial = EnumHelper.addToolMaterial("TrilobiteSwordMaterial", 0, 700, 0, 8F, 12);
	public static final Item.ToolMaterial TurquoiseSwordMaterial = EnumHelper.addToolMaterial("TurquoiseSwordMaterial", 0, 280, 0, 5F, 12);
	public static final Item.ToolMaterial SharkSwordMaterial = EnumHelper.addToolMaterial("SharkSwordMaterial", 0, 280, 0, 5F, 12);
	public static final Item.ToolMaterial BerylSwordMaterial = EnumHelper.addToolMaterial("BerylSwordMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial GarnetSwordMaterial = EnumHelper.addToolMaterial("GarnetSwordMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial JasperSwordMaterial = EnumHelper.addToolMaterial("JasperSwordMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial OpalSwordMaterial = EnumHelper.addToolMaterial("OpalSwordMaterial",2,192,5.0F,1.5F,12);
	public static final Item.ToolMaterial HematiteSwordMaterial = EnumHelper.addToolMaterial("HematiteSwordMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial PyriteSwordMaterial = EnumHelper.addToolMaterial("PyriteSwordMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial CoralSwordMaterial = EnumHelper.addToolMaterial("CoralSwordMaterial", 2, 192, 5.0F, 1.5F, 12); // We need to set values


	//FishingRod Materials
	public static final Item.ToolMaterial WoodFishingRodMaterial = EnumHelper.addToolMaterial("WoodFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial StoneFishingRodMaterial = EnumHelper.addToolMaterial("StoneFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial IronFishingRodMaterial = EnumHelper.addToolMaterial("IronFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial GoldFishingRodMaterial = EnumHelper.addToolMaterial("GoldFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial DiamondFishingRodMaterial = EnumHelper.addToolMaterial("DiamondFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);
	public static final Item.ToolMaterial EmeraldFishingRodMaterial = EnumHelper.addToolMaterial("EmeraldFishingRodMaterial", 2, 192, 5.0F, 1.5F, 12);


	//Tools
	//Ammonite
	public static Item AmmoniteAxe;
	public static Item AmmoniteHoe;
	public static Item AmmonitePickaxe;
	public static Item AmmoniteShovel;
	public static Item AmmoniteSword;
	//Aquamarine
	public static Item AquamarineAxe;
	public static Item AquamarineHoe;
	public static Item AquamarinePickaxe;
	public static Item AquamarineShovel;	
	public static Item AquamarineSword;
	//Diopside
	public static Item DiopsideAxe;
	public static Item DiopsideHoe;
	public static Item DiopsidePickaxe;
	public static Item DiopsideShovel;
	public static Item DiopsideSword;
	//Fluorite
	public static Item FluoriteAxe;
	public static Item FluoriteHoe;
	public static Item FluoritePickaxe;
	public static Item FluoriteShovel;
	public static Item FluoriteSword;
	//Trilobite
	public static Item TrilobiteAxe;
	public static Item TrilobiteHoe;
	public static Item TrilobitePickaxe;
	public static Item TrilobiteShovel;
	public static Item TrilobiteSword;
	//Turquoise
	public static Item TurquoiseAxe;
	public static Item TurquoiseHoe;
	public static Item TurquoisePickaxe;
	public static Item TurquoiseShovel;
	public static Item TurquoiseSword;
	//Shark
	public static Item SharkAxe;
	public static Item SharkHoe;
	public static Item SharkPickaxe;
	public static Item SharkShovel;
	public static Item SharkSword;
	//Beryl
	public static Item BerylAxe;
	public static Item BerylHoe;
	public static Item BerylPickaxe;
	public static Item BerylShovel;
	public static Item BerylSword;
	//Garnet
	public static Item GarnetAxe;
	public static Item GarnetHoe;
	public static Item GarnetPickaxe;
	public static Item GarnetShovel;
	public static Item GarnetSword;
	//Jasper
	public static Item JasperAxe;
	public static Item JasperHoe;
	public static Item JasperPickaxe;
	public static Item JasperShovel;
	public static Item JasperSword;
	//Opal
	public static Item OpalAxe;
	public static Item OpalHoe;
	public static Item OpalPickaxe;
	public static Item OpalShovel;
	public static Item OpalSword;
	//Hematite
	public static Item HematiteAxe;
	public static Item HematiteHoe;
	public static Item HematitePickaxe;
	public static Item HematiteShovel;
	public static Item HematiteSword;
	//Pyrite
	public static Item PyriteAxe;
	public static Item PyriteHoe;
	public static Item PyritePickaxe;
	public static Item PyriteShovel;
	public static Item PyriteSword;

	//Coral
	public static Item CoralAxe;
	public static Item CoralHoe;
	public static Item CoralPickaxe;
	public static Item CoralShovel;
	public static Item CoralSword;

	//Armor
	//Ammonite
	//Ids
	public static int AmmoniteHelmId;
	public static int AmmoniteChestId;
	public static int AmmoniteLegsId;
	public static int AmmoniteBootsId;
	//Items
	public static Item AmmoniteHelm;
	public static Item AmmoniteChest;
	public static Item AmmoniteLegs;
	public static Item AmmoniteBoots;
	//Aquamarine
	//Ids
	public static int AquamarineHelmId;
	public static int AquamarineChestId;
	public static int AquamarineLegsId;
	public static int AquamarineBootsId;
	//Items
	public static Item AquamarineHelm;
	public static Item AquamarineChest;
	public static Item AquamarineLegs;
	public static Item AquamarineBoots;
	//Diopside
	//Ids
	public static int DiopsideHelmId;
	public static int DiopsideChestId;
	public static int DiopsideLegsId;
	public static int DiopsideBootsId;
	//Items
	public static Item DiopsideHelm;
	public static Item DiopsideChest;
	public static Item DiopsideLegs;
	public static Item DiopsideBoots;	
	//Fluorite
	//Ids
	public static int FluoriteHelmId;
	public static int FluoriteChestId;
	public static int FluoriteLegsId;
	public static int FluoriteBootsId;
	//Items
	public static Item FluoriteHelm;
	public static Item FluoriteChest;
	public static Item FluoriteLegs;
	public static Item FluoriteBoots;	
	//Trilobite
	//Ids
	public static int TrilobiteHelmId;
	public static int TrilobiteChestId;
	public static int TrilobiteLegsId;
	public static int TrilobiteBootsId;
	//Items
	public static Item TrilobiteHelm;
	public static Item TrilobiteChest;
	public static Item TrilobiteLegs;
	public static Item TrilobiteBoots;
	//Turquoise
	//Ids
	public static int TurquoiseHelmId;
	public static int TurquoiseChestId;
	public static int TurquoiseLegsId;
	public static int TurquoiseBootsId;
	//Items
	public static Item TurquoiseHelm;
	public static Item TurquoiseChest;
	public static Item TurquoiseLegs;
	public static Item TurquoiseBoots;
	//Shark Armor
	//Ids
	public static int SharkHelmId;
	public static int SharkChestId;
	public static int SharkLegsId;
	public static int SharkBootsId;
	//Items
	public static Item SharkHelm;
	public static Item SharkChest;
	public static Item SharkLegs;
	public static Item SharkBoots;
	//Beryl
	//Ids
	public static int BerylHelmId;
	public static int BerylChestId;
	public static int BerylLegsId;
	public static int BerylBootsId;
	//Items
	public static Item BerylHelm;
	public static Item BerylChest;
	public static Item BerylLegs;
	public static Item BerylBoots;
	//Garnet
	//Ids
	public static int GarnetHelmId;
	public static int GarnetChestId;
	public static int GarnetLegsId;
	public static int GarnetBootsId;
	//Items
	public static Item GarnetHelm;
	public static Item GarnetChest;
	public static Item GarnetLegs;
	public static Item GarnetBoots;
	//Jasper
	//Ids
	public static int JasperHelmId;
	public static int JasperChestId;
	public static int JasperLegsId;
	public static int JasperBootsId;
	//Items
	public static Item JasperHelm;
	public static Item JasperChest;
	public static Item JasperLegs;
	public static Item JasperBoots;
	//Opal
	//Ids
	public static int OpalHelmId;
	public static int OpalChestId;
	public static int OpalLegsId;
	public static int OpalBootsId;
	//Items
	public static Item OpalHelm;
	public static Item OpalChest;
	public static Item OpalLegs;
	public static Item OpalBoots;
	//Hematite
	//Ids
	public static int HematiteHelmId;
	public static int HematiteChestId;
	public static int HematiteLegsId;
	public static int HematiteBootsId;

	//Items
	public static Item HematiteHelm;
	public static Item HematiteChest;
	public static Item HematiteLegs;
	public static Item HematiteBoots;
	//Pyrite
	//Ids
	public static int PyriteHelmId;
	public static int PyriteChestId;
	public static int PyriteLegsId;
	public static int PyriteBootsId;
	//Items
	public static Item PyriteHelm;
	public static Item PyriteChest;
	public static Item PyriteLegs;
	public static Item PyriteBoots;

	//Gems
	//Ocean
	public static Item AmmoniteGem;
	public static Item AquamarineGem;
	public static Item DiopsideGem;
	public static Item FluoriteGem;
	public static Item TrilobiteGem;
	public static Item TurquoiseGem;
	//River
	public static Item BerylGem;
	public static Item GarnetGem;
	public static Item JasperGem;
	public static Item OpalGem;
	//Lake
	public static Item HematiteGem;
	public static Item PyriteGem;
	//FishingRods
	//Vanilla Ores
	public static Item WoodFishingRod;
	public static Item StoneFishingRod;
	public static Item IronFishingRod;
	public static Item GoldFishingRod;
	public static Item DiamondFishingRod;
	public static Item EmeraldFishingRod;
	//Food Items
	//Raw
	public static Item RawAnchovy;
	public static Item RawCod;
	public static Item RawHaddock;
	public static Item RawHerring;
	public static Item RawMullet;
	public static Item RawPlaice;
	public static Item RawSalmon;
	public static Item RawSeaBass;
	public static Item RawSharkFin;
	public static Item RawSwordfish;
	public static Item RawTrout;
	public static Item RawTuna;
	//Cooked
	public static Item CookedAnchovy;
	public static Item CookedCod;
	public static Item CookedHaddock;
	public static Item CookedHerring;
	public static Item CookedMullet;
	public static Item CookedPlaice;
	public static Item CookedSalmon;
	public static Item CookedSeaBass;
	public static Item CookedSharkFin;
	public static Item CookedSwordFish;
	public static Item CookedTrout;
	public static Item CookedTuna;
	//Other Fish Drops/Items
	public static Item SharkTooth;
	public static Item PurifiedWater;

	//Machines
	//Furnace
	//Idle
	//	public static Block AmmoniteFurnace_Idle;
	//	public static Block AquamarineFurnace_Idle;
	//	public static Block DiopsideFurnace_Idle;
	//	public static Block FluoriteFurnace_Idle;
	//	public static Block TrilobiteFurnace_Idle;
	//	public static Block TurquoiseFurnace_Idle;
	//	//Active
	//	public static Block AmmoniteFurnace_Active;
	//	public static Block AquamarineFurnace_Active;
	//	public static Block DiopsideFurnace_Active;
	//	public static Block FluoriteFurnace_Active;
	//	public static Block TrilobiteFurnace_Active;
	//	public static Block TurquoiseFurnace_Active;

	//Blocks
	public static Block AmmoniteBlock;
	public static Block AquamarineBlock;
	public static Block DiopsideBlock;
	public static Block FluoriteBlock;
	public static Block TrilobiteBlock;
	public static Block TurquoiseBlock;

	public static Block BerylBlock;
	public static Block GarnetBlock;
	public static Block JasperBlock;
	public static Block OpalBlock;

	public static Block HematiteBlock;
	public static Block PyriteBlock;

	//Bricks
	public static Block AmmoniteBrick;
	public static Block AquamarineBrick;
	public static Block DiopsideBrick;
	public static Block FluoriteBrick;
	public static Block TrilobiteBrick;
	public static Block TurquoiseBrick;

	public static Block BerylBrick;
	public static Block GarnetBrick;
	public static Block JasperBrick;
	public static Block OpalBrick;

	public static Block HematiteBrick;
	public static Block PyriteBrick;

	//Decoratives
	public static Block AmmoniteDecorative;
	public static Block AquamarineDecorative;
	public static Block DiopsideDecorative;
	public static Block FluoriteDecorative;
	public static Block TrilobiteDecorative;
	public static Block TurquoiseDecorative;

	public static Block BerylDecorative;
	public static Block GarnetDecorative;
	public static Block JasperDecorative;
	public static Block OpalDecorative;

	public static Block HematiteDecorative;
	public static Block PyriteDecorative;


	//Pillars
	public static Block AmmonitePillar;
	public static Block AquamarinePillar;
	public static Block DiopsidePillar;
	public static Block FluoritePillar;
	public static Block TrilobitePillar;
	public static Block TurquoisePillar;

	public static Block BerylPillar;
	public static Block GarnetPillar;
	public static Block JasperPillar;
	public static Block OpalPillar;

	public static Block HematitePillar;
	public static Block PyritePillar;

	//Equipment
	public static Item ScubaHelm;
	public static Item ScubaChest;
	public static Item ScubaLegs;
	public static Item ScubaBoots;

	//Seaweed
	public static Item SeaWeed;
//	public static Item SeaWeedHandle; //Why Do we need this? In the crafting won't the seaweed item just replace the sticks?^^

    //Coral
    public static Block CoralBlock;

	//Instance
	@Mod.Instance(References.MODID)
	public static FishMod instance;

	@EventHandler
	public void PreInit(FMLPreInitializationEvent preEvent)
	{ 
		//Creative Tab
		fishmod = new CreativeTabs(References.MODID){
			@SideOnly(Side.CLIENT)
			public Item getTabIconItem(){
				int id = Item.getIdFromItem(SharkSword);
				return Item.getItemById(id);
			}

		};

		/* EXPAND ON PUBLIC HERE */
		//Mobs
		//registerEntity(TunaFish.class,"TunaFish");
		//Water Ores
		//Ocean
		AmmoniteOre = new OceanOre(Material.rock).setHardness(4).setBlockName("AmmoniteOre").setBlockTextureName("fishmod:Ammonite Ore").setCreativeTab(fishmod);
		AquamarineOre = new AquamarineOreClass(Material.rock).setHardness(5).setBlockName("AquamarineOre").setBlockTextureName("fishmod:Aquamarine Ore").setCreativeTab(fishmod);
		DiopsideOre = new OceanOre(Material.rock).setHardness(6).setBlockName("DiopsideOre").setBlockTextureName("fishmod:Diopside Ore").setCreativeTab(fishmod);
		FluoriteOre = new FluoriteOreClass(Material.rock).setHardness(7).setBlockName("FluoriteOre").setBlockTextureName("fishmod:Fluorite Ore").setCreativeTab(fishmod);
		TrilobiteOre = new TrilobiteOreClass(Material.rock).setHardness(8).setBlockName("TrilobiteOre").setBlockTextureName("fishmod:Trilobite Ore").setCreativeTab(fishmod);
		TurquoiseOre = new TuruquoiseOreClass(Material.rock).setHardness(9).setBlockName("TurquoiseOre").setBlockTextureName("fishmod:Turquoise Ore").setCreativeTab(fishmod);
		//River
		BerylOre = new RiverOre(Material.rock).setHardness(6).setBlockName("BerylOre").setBlockTextureName("fishmod:Beryl Ore").setCreativeTab(fishmod);
		GarnetOre = new RiverOre(Material.rock).setHardness(7).setBlockName("GarnetOre").setBlockTextureName("fishmod:Garnet Ore").setCreativeTab(fishmod);
		JasperOre = new RiverOre(Material.rock).setHardness(7).setBlockName("JasperOre").setBlockTextureName("fishmod:Jasper Ore").setCreativeTab(fishmod);
		OpalOre = new RiverOre(Material.rock).setHardness(6).setBlockName("OpalOre").setBlockTextureName("fishmod:Opal Ore").setCreativeTab(fishmod);

		//Lake
		HematiteOre = new LakeOre(Material.rock).setHardness(5).setBlockName("HematiteOre").setBlockTextureName("fishmod:Hematite Ore").setCreativeTab(fishmod);
		PyriteOre = new LakeOre(Material.rock).setHardness(6).setBlockName("PyriteOre").setBlockTextureName("fishmod:Pyrite Ore").setCreativeTab(fishmod);

		//Tools
		// Ammonite Tools
		AmmoniteAxe = new AmmoniteAxe(Ammonite).setUnlocalizedName("Ammonite Axe").setTextureName("fishmod:Ammonite Axe").setCreativeTab(fishmod);
		AmmoniteHoe = new AmmoniteHoe(Ammonite).setUnlocalizedName("Ammonite Hoe").setTextureName("fishmod:Ammonite Hoe").setCreativeTab(fishmod);
		AmmonitePickaxe = new AmmonitePickaxe(AmmonitePickaxeMaterial).setUnlocalizedName("Ammonite Pickaxe").setTextureName("fishmod:Ammonite Pickaxe").setCreativeTab(fishmod);
		AmmoniteShovel = new AmmoniteShovel(Ammonite).setUnlocalizedName("Ammonite Shovel").setTextureName("fishmod:Ammonite Shovel").setCreativeTab(fishmod);
		AmmoniteSword = new AmmoniteSword(AmmoniteSwordMaterial).setUnlocalizedName("Ammonite Sword").setTextureName("fishmod:Ammonite Sword").setCreativeTab(fishmod);

		// Aquamarine Tools
		AquamarineAxe = new AquamarineAxe(Aquamarine).setUnlocalizedName("Aquamarine Axe").setTextureName("fishmod:Aquamarine Axe").setCreativeTab(fishmod);
		AquamarineHoe = new AquamarineHoe(Aquamarine).setUnlocalizedName("Aquamarine Hoe").setTextureName("fishmod:Aquamarine Hoe").setCreativeTab(fishmod);
		AquamarinePickaxe = new AquamarinePickaxe(AquamarinePickaxeMaterial).setUnlocalizedName("Aquamarine Pickaxe").setTextureName("fishmod:Aquamarine Pickaxe").setCreativeTab(fishmod);
		AquamarineShovel = new AquamarineShovel(Aquamarine).setUnlocalizedName("Aquamarine Shovel").setTextureName("fishmod:Aquamarine Shovel").setCreativeTab(fishmod);
		AquamarineSword = new AquamarineSword(AquamarineSwordMaterial).setUnlocalizedName("Aquamarine Sword").setTextureName("fishmod:Aquamarine Sword").setCreativeTab(fishmod);

		// Diopside Tools
		DiopsideAxe = new DiopsideAxe(Diopside).setUnlocalizedName("Diopside Axe").setTextureName("fishmod:Diopside Axe").setCreativeTab(fishmod);
		DiopsideHoe = new DiopsideHoe(Diopside).setUnlocalizedName("Diopside Hoe").setTextureName("fishmod:Diopside Hoe").setCreativeTab(fishmod);
		DiopsidePickaxe = new DiopsidePickaxe(DiopsidePickaxeMaterial).setUnlocalizedName("Diopside Pickaxe").setTextureName("fishmod:Diopside Pickaxe").setCreativeTab(fishmod);
		DiopsideShovel = new com.z3t0zlabz.fishmod.items.tools.DiopsideShovel(Diopside).setUnlocalizedName("Diopside Shovel").setTextureName("fishmod:Diopside Shovel").setCreativeTab(fishmod);
		DiopsideSword = new DiopsideSword(DiopsideSwordMaterial).setUnlocalizedName("Diopside Sword").setTextureName("fishmod:Diopside Sword").setCreativeTab(fishmod);

		//Fluorite Tools
		FluoriteAxe = new FluoriteAxe(Fluorite).setUnlocalizedName("Fluorite Axe").setTextureName("fishmod:Fluorite Axe").setCreativeTab(fishmod);
		FluoriteHoe = new FluoriteHoe(Fluorite).setUnlocalizedName("Fluorite Hoe").setTextureName("fishmod:Fluorite Hoe").setCreativeTab(fishmod);
		FluoritePickaxe = new FluoritePickaxe(FluoritePickaxeMaterial).setUnlocalizedName("Fluorite Pickaxe").setTextureName("fishmod:Fluorite Pickaxe").setCreativeTab(fishmod);
		FluoriteShovel = new FluoriteShovel(Fluorite).setUnlocalizedName("Fluorite Shovel").setTextureName("fishmod:Fluorite Shovel").setCreativeTab(fishmod);
		FluoriteSword = new FluoriteSword(FluoriteSwordMaterial).setUnlocalizedName("Fluorite Sword").setTextureName("fishmod:Fluorite Sword").setCreativeTab(fishmod);

		//Trilobite Tools
		TrilobiteAxe = new TrilobiteAxe(Trilobite).setUnlocalizedName("Trilobite Axe").setTextureName("fishmod:Trilobite Axe").setCreativeTab(fishmod);
		TrilobiteHoe = new TrilobiteHoe(Trilobite).setUnlocalizedName("Trilobite Hoe").setTextureName("fishmod:Trilobite Hoe").setCreativeTab(fishmod);
		TrilobitePickaxe = new TrilobitePickaxe(TrilobitePickaxeMaterial).setUnlocalizedName("Trilobite Pickaxe").setTextureName("fishmod:Trilobite Pickaxe").setCreativeTab(fishmod);
		TrilobiteShovel = new TrilobiteShovel(Trilobite).setUnlocalizedName("Trilobite Shovel").setTextureName("fishmod:Trilobite Shovel").setCreativeTab(fishmod);
		TrilobiteSword = new TrilobiteSword(TrilobiteSwordMaterial).setUnlocalizedName("Trilobite Sword").setTextureName("fishmod:Trilobite Sword").setCreativeTab(fishmod);

		//Turquoise Tools
		TurquoiseAxe = new TurquoiseAxe(Turquoise).setUnlocalizedName("Turquoise Axe").setTextureName("fishmod:Turquoise Axe").setCreativeTab(fishmod);
		TurquoiseHoe = new TurquoiseHoe(Turquoise).setUnlocalizedName("Turquoise Hoe").setTextureName("fishmod:Turquoise Hoe").setCreativeTab(fishmod);
		TurquoisePickaxe = new TurquoisePickaxe(TurquoisePickaxeMaterial).setUnlocalizedName("Turquoise Pickaxe").setTextureName("fishmod:Turquoise Pickaxe").setCreativeTab(fishmod);
		TurquoiseShovel = new TurquoiseShovel(Turquoise).setUnlocalizedName("Turquoise Shovel").setTextureName("fishmod:Turquoise Shovel").setCreativeTab(fishmod);
		TurquoiseSword = new TurquoiseSword(TurquoiseSwordMaterial).setUnlocalizedName("Turquoise Sword").setTextureName("fishmod:Turquoise Sword").setCreativeTab(fishmod);
		//Shark Tools
		SharkAxe = new SharkAxe(Shark).setUnlocalizedName("Shark Axe").setTextureName("fishmod:Shark Axe").setCreativeTab(fishmod);
		SharkHoe = new SharkHoe(Shark).setUnlocalizedName("Shark Hoe").setTextureName("fishmod:Shark Hoe").setCreativeTab(fishmod);
		SharkPickaxe = new SharkPickaxe(SharkPickaxeMaterial).setUnlocalizedName("Shark Pickaxe").setTextureName("fishmod:Shark Pickaxe").setCreativeTab(fishmod);
		SharkShovel = new SharkShovel(Shark).setUnlocalizedName("Shark Shovel").setTextureName("fishmod:Shark Shovel").setCreativeTab(fishmod);
		SharkSword = new SharkSword(SharkSwordMaterial).setUnlocalizedName("Shark Sword").setTextureName("fishmod:Shark Sword").setCreativeTab(fishmod);
		//Beryl
		BerylAxe = new BerylAxe(Beryl).setUnlocalizedName("Beryl Axe").setTextureName("fishmod:Beryl Axe").setCreativeTab(fishmod);
		BerylHoe = new BerylHoe(Beryl).setUnlocalizedName("Beryl Hoe").setTextureName("fishmod:Beryl Hoe").setCreativeTab(fishmod);
		BerylPickaxe = new BerylPickaxe(BerylPickaxeMaterial).setUnlocalizedName("Beryl Pickaxe").setTextureName("fishmod:Beryl Pickaxe").setCreativeTab(fishmod);
		BerylShovel = new BerylShovel(Beryl).setUnlocalizedName("Beryl Shovel").setTextureName("fishmod:Beryl Shovel").setCreativeTab(fishmod);
		BerylSword = new BerylSword(BerylSwordMaterial).setUnlocalizedName("Beryl Sword").setTextureName("fishmod:Beryl Sword").setCreativeTab(fishmod);
		//Garnet
		GarnetAxe = new GarnetAxe(Garnet).setUnlocalizedName("Garnet Axe").setTextureName("fishmod:Garnet Axe").setCreativeTab(fishmod);
		GarnetHoe = new GarnetHoe(GarnetPickaxeMaterial).setUnlocalizedName("Garnet Hoe").setTextureName("fishmod:Garnet Hoe").setCreativeTab(fishmod);
		GarnetPickaxe = new GarnetPickaxe(GarnetPickaxeMaterial).setUnlocalizedName("Garnet Pickaxe").setTextureName("fishmod:Garnet Pickaxe").setCreativeTab(fishmod);
		GarnetShovel = new GarnetShovel(Garnet).setUnlocalizedName("Garnet Shovel").setTextureName("fishmod:Garnet Shovel").setCreativeTab(fishmod);
		GarnetSword = new GarnetSword(GarnetSwordMaterial).setUnlocalizedName("Garnet Sword").setTextureName("fishmod:Garnet Sword").setCreativeTab(fishmod);
		//Jasper
		JasperAxe = new JasperAxe(Jasper).setUnlocalizedName("Jasper Axe").setTextureName("fishmod:Jasper Axe").setCreativeTab(fishmod);
		JasperHoe = new JasperHoe(Jasper).setUnlocalizedName("Jasper Hoe").setTextureName("fishmod:Jasper Hoe").setCreativeTab(fishmod);
		JasperPickaxe = new JasperPickaxe(JasperPickaxeMaterial).setUnlocalizedName("Jasper Pickaxe").setTextureName("fishmod:Jasper Pickaxe").setCreativeTab(fishmod);
		JasperShovel = new JasperShovel(Beryl).setUnlocalizedName("Jasper Shovel").setTextureName("fishmod:Jasper Shovel").setCreativeTab(fishmod);
		JasperSword = new JasperSword(JasperSwordMaterial).setUnlocalizedName("Jasper Sword").setTextureName("fishmod:Jasper Sword").setCreativeTab(fishmod);
		//Opal
		OpalAxe = new OpalAxe(Opal).setUnlocalizedName("Opal Axe").setTextureName("fishmod:Opal Axe").setCreativeTab(fishmod);
		OpalHoe = new OpalHoe(Opal).setUnlocalizedName("Opal Hoe").setTextureName("fishmod:Opal Hoe").setCreativeTab(fishmod);
		OpalPickaxe = new OpalPickaxe(OpalPickaxeMaterial).setUnlocalizedName("Opal Pickaxe").setTextureName("fishmod:Opal Pickaxe").setCreativeTab(fishmod);
		OpalShovel = new OpalShovel(Opal).setUnlocalizedName("Opal Shovel").setTextureName("fishmod:Opal Shovel").setCreativeTab(fishmod);
		OpalSword = new OpalSword(OpalSwordMaterial).setUnlocalizedName("Opal Sword").setTextureName("fishmod:Opal Sword").setCreativeTab(fishmod);
		//Hematite
		HematiteAxe = new HematiteAxe(Hematite).setUnlocalizedName("Hematite Axe").setTextureName("fishmod:Hematite Axe").setCreativeTab(fishmod);
		HematiteHoe = new HematiteHoe(Hematite).setUnlocalizedName("Hematite Hoe").setTextureName("fishmod:Hematite Hoe").setCreativeTab(fishmod);
		HematitePickaxe = new HematitePickaxe(HematitePickaxeMaterial).setUnlocalizedName("Hematite Pickaxe").setTextureName("fishmod:Hematite Pickaxe").setCreativeTab(fishmod);
		HematiteShovel = new HematiteShovel(Hematite).setUnlocalizedName("Hematite Shovel").setTextureName("fishmod:Hematite Shovel").setCreativeTab(fishmod);
		HematiteSword = new HematiteSword(HematiteSwordMaterial).setUnlocalizedName("Hematite Sword").setTextureName("fishmod:Hematite Sword").setCreativeTab(fishmod);
		//Pyrite
		PyriteAxe = new PyriteAxe(Pyrite).setUnlocalizedName("Pyrite Axe").setTextureName("fishmod:Pyrite Axe").setCreativeTab(fishmod);
		PyriteHoe = new PyriteHoe(Pyrite).setUnlocalizedName("Pyrite Hoe").setTextureName("fishmod:Pyrite Hoe").setCreativeTab(fishmod);
		PyritePickaxe = new PyritePickaxe(PyritePickaxeMaterial).setUnlocalizedName("Pyrite Pickaxe").setTextureName("fishmod:Pyrite Pickaxe").setCreativeTab(fishmod);
		PyriteShovel = new PyriteShovel(Pyrite).setUnlocalizedName("Pyrite Shovel").setTextureName("fishmod:Pyrite Shovel").setCreativeTab(fishmod);
		PyriteSword = new PyriteSword(PyriteSwordMaterial).setUnlocalizedName("Pyrite Sword").setTextureName("fishmod:Pyrite Sword").setCreativeTab(fishmod);
		// Coral
		CoralAxe = new CoralAxe(Coral).setUnlocalizedName("Coral Axe").setTextureName("fishmod:Coral Axe").setCreativeTab(fishmod);
		CoralHoe = new CoralHoe(Coral).setUnlocalizedName("Coral Hoe").setTextureName("fishmod:Coral Hoe").setCreativeTab(fishmod);
		CoralPickaxe = new CoralPickaxe(CoralPickaxeMaterial).setUnlocalizedName("Coral Pickaxe").setTextureName("fishmod:Coral Pickaxe").setCreativeTab(fishmod);
		CoralShovel = new CoralShovel(Coral).setUnlocalizedName("Coral Shovel").setTextureName("fishmod:Coral Shovel").setCreativeTab(fishmod);
		CoralSword = new CoralSword(CoralSwordMaterial).setUnlocalizedName("Coral Sword").setTextureName("fishmod:Coral Sword").setCreativeTab(fishmod);

		//Armor
		//Ammonite Armor
		//ArmorMaterial AmmoniteArmor = EnumHelper.addArmorMaterial(name, durability, reductionAmounts, enchantability)
		ArmorMaterial AmmoniteArmorMaterial = EnumHelper.addArmorMaterial("AmmoniteArmor",400 , new int[] {4, 6 , 4 , 4}, 10);
		AmmoniteHelm = (new AmmoniteArmor(AmmoniteArmorMaterial,AmmoniteHelmId, 0)).setUnlocalizedName("Ammonite Helm");
		AmmoniteChest = (new AmmoniteArmor(AmmoniteArmorMaterial, AmmoniteChestId, 1)).setUnlocalizedName("Ammonite Chest");
		AmmoniteLegs = (new AmmoniteArmor(AmmoniteArmorMaterial, AmmoniteLegsId, 2)).setUnlocalizedName("Ammonite Legs");
		AmmoniteBoots = (new AmmoniteArmor(AmmoniteArmorMaterial, AmmoniteBootsId, 3)).setUnlocalizedName("Ammonite Boots");
		//Aquamarine Armor
		ArmorMaterial AquamarineArmorMaterial = EnumHelper.addArmorMaterial("AquamarineArmor", 300, new int[] {2, 6 , 6 , 2}, 10);
		AquamarineHelm = (new AquamarineArmor(AquamarineArmorMaterial,AquamarineHelmId , 0)).setUnlocalizedName("Aquamarine Helm");
		AquamarineChest = (new AquamarineArmor(AquamarineArmorMaterial, AquamarineChestId, 1)).setUnlocalizedName("Aquamarine Chest");
		AquamarineLegs = (new AquamarineArmor(AquamarineArmorMaterial, AquamarineLegsId, 2)).setUnlocalizedName("Aquamarine Legs");
		AquamarineBoots = (new AquamarineArmor(AquamarineArmorMaterial, AquamarineBootsId, 3)).setUnlocalizedName("Aquamarine Boots");
		//Diopside Armor
		ArmorMaterial DiopsideArmorMaterial = EnumHelper.addArmorMaterial("DiopsideArmor", 530, new int[] {4, 4 , 6 , 4}, 10);
		DiopsideHelm = (new DiopsideArmor(DiopsideArmorMaterial,DiopsideHelmId , 0)).setUnlocalizedName("Diopside Helm");
		DiopsideChest = (new DiopsideArmor(DiopsideArmorMaterial, DiopsideChestId, 1)).setUnlocalizedName("Diopside Chest");
		DiopsideLegs = (new DiopsideArmor(DiopsideArmorMaterial, DiopsideLegsId, 2)).setUnlocalizedName("Diopside Legs");
		DiopsideBoots = (new DiopsideArmor(DiopsideArmorMaterial, DiopsideBootsId, 3)).setUnlocalizedName("Diopside Boots");
		//Fluorite Armor
		ArmorMaterial FluoriteArmorMaterial = EnumHelper.addArmorMaterial("FluoriteArmor", 200, new int[]{2, 4, 4, 2}, 10);
		FluoriteHelm = (new FluoriteArmor(FluoriteArmorMaterial,FluoriteHelmId , 0)).setUnlocalizedName("Fluorite Helm");
		FluoriteChest = (new FluoriteArmor(FluoriteArmorMaterial, FluoriteChestId, 1)).setUnlocalizedName("Fluorite Chest");
		FluoriteLegs = (new FluoriteArmor(FluoriteArmorMaterial, FluoriteLegsId, 2)).setUnlocalizedName("Fluorite Legs");
		FluoriteBoots = (new FluoriteArmor(FluoriteArmorMaterial, FluoriteBootsId, 3)).setUnlocalizedName("Fluorite Boots");
		//Trilobite Armor
		ArmorMaterial TrilobiteArmorMaterial = EnumHelper.addArmorMaterial("TrilobiteArmor", 370, new int[]{4, 4, 4, 4}, 10);
		TrilobiteHelm = (new TrilobiteArmor(TrilobiteArmorMaterial,TrilobiteHelmId , 0)).setUnlocalizedName("Trilobite Helm");
		TrilobiteChest = (new TrilobiteArmor(TrilobiteArmorMaterial, TrilobiteChestId, 1)).setUnlocalizedName("Trilobite Chest");
		TrilobiteLegs = (new TrilobiteArmor(TrilobiteArmorMaterial, TrilobiteLegsId, 2)).setUnlocalizedName("Trilobite Legs");
		TrilobiteBoots = (new TrilobiteArmor(TrilobiteArmorMaterial, TrilobiteBootsId, 3)).setUnlocalizedName("Trilobite Boots");
		//Turquoise Armor
		ArmorMaterial TurquoiseArmorMaterial = EnumHelper.addArmorMaterial("TurquoiseArmor", 240, new int[]{2, 4, 2, 2}, 10);
		TurquoiseHelm = (new TurquoiseArmor(TurquoiseArmorMaterial,TurquoiseHelmId , 0)).setUnlocalizedName("Turquoise Helm");
		TurquoiseChest = (new TurquoiseArmor(TurquoiseArmorMaterial, TurquoiseChestId, 1)).setUnlocalizedName("Turquoise Chest");
		TurquoiseLegs = (new TurquoiseArmor(TurquoiseArmorMaterial, TurquoiseLegsId, 2)).setUnlocalizedName("Turquoise Legs");
		TurquoiseBoots = (new TurquoiseArmor(TurquoiseArmorMaterial, TurquoiseBootsId, 3)).setUnlocalizedName("Turquoise Boots");
		//Shark Armor
		ArmorMaterial SharkArmorMaterial = EnumHelper.addArmorMaterial("SharkArmor", 240, new int[]{4, 8, 4, 4}, 10);
		SharkHelm = (new SharkArmor(SharkArmorMaterial,SharkHelmId , 0)).setUnlocalizedName("Shark Helm");
		SharkChest = (new SharkArmor(SharkArmorMaterial, SharkChestId, 1)).setUnlocalizedName("Shark Chest");
		SharkLegs = (new SharkArmor(SharkArmorMaterial, SharkLegsId, 2)).setUnlocalizedName("Shark Legs");
		SharkBoots = (new SharkArmor(SharkArmorMaterial, SharkBootsId, 3)).setUnlocalizedName("Shark Boots");
		//Beryl Armor
		ArmorMaterial BerylArmorMaterial = EnumHelper.addArmorMaterial("BerylArmor", 240, new int[]{2, 4, 2, 2}, 10);
		BerylHelm = (new BerylArmor(BerylArmorMaterial,BerylHelmId , 0)).setUnlocalizedName("Beryl Helm");
		BerylChest = (new BerylArmor(BerylArmorMaterial, BerylChestId, 1)).setUnlocalizedName("Beryl Chest");
		BerylLegs = (new BerylArmor(BerylArmorMaterial, BerylLegsId, 2)).setUnlocalizedName("Beryl Legs");
		BerylBoots = (new BerylArmor(BerylArmorMaterial, BerylBootsId, 3)).setUnlocalizedName("Beryl Boots");
		//Garnet Armor
		ArmorMaterial GarnetArmorMaterial = EnumHelper.addArmorMaterial("GarnetArmor", 240, new int[]{2, 4, 2, 2}, 10);
		GarnetHelm = (new GarnetArmor(GarnetArmorMaterial,GarnetHelmId , 0)).setUnlocalizedName("Garnet Helm");
		GarnetChest = (new GarnetArmor(GarnetArmorMaterial, GarnetChestId, 1)).setUnlocalizedName("Garnet Chest");
		GarnetLegs = (new GarnetArmor(GarnetArmorMaterial, GarnetLegsId, 2)).setUnlocalizedName("Garnet Legs");
		GarnetBoots = (new GarnetArmor(GarnetArmorMaterial, GarnetBootsId, 3)).setUnlocalizedName("Garnet Boots");
		//Jasper Armor
		ArmorMaterial JasperArmorMaterial = EnumHelper.addArmorMaterial("JasperArmor", 240, new int[]{2, 4, 2, 2}, 10);
		JasperHelm = (new JasperArmor(JasperArmorMaterial,JasperHelmId , 0)).setUnlocalizedName("Jasper Helm");
		JasperChest = (new JasperArmor(JasperArmorMaterial, JasperChestId, 1)).setUnlocalizedName("Jasper Chest");
		JasperLegs = (new JasperArmor(JasperArmorMaterial, JasperLegsId, 2)).setUnlocalizedName("Jasper Legs");
		JasperBoots = (new JasperArmor(JasperArmorMaterial, JasperBootsId, 3)).setUnlocalizedName("Jasper Boots");
		//Opal Armor
		ArmorMaterial OpalArmorMaterial = EnumHelper.addArmorMaterial("OpalArmor", 240, new int[]{2, 4, 2, 2}, 10);
		OpalHelm = (new OpalArmor(OpalArmorMaterial,OpalHelmId , 0)).setUnlocalizedName("Opal Helm");
		OpalChest = (new OpalArmor(OpalArmorMaterial, OpalChestId, 1)).setUnlocalizedName("Opal Chest");
		OpalLegs = (new OpalArmor(OpalArmorMaterial, OpalLegsId, 2)).setUnlocalizedName("Opal Legs");
		OpalBoots = (new OpalArmor(OpalArmorMaterial, OpalBootsId, 3)).setUnlocalizedName("Opal Boots");
		//Hematite Armor
		ArmorMaterial HematiteArmorMaterial = EnumHelper.addArmorMaterial("HematiteArmor", 240, new int[]{2, 4, 2, 2}, 10);
		HematiteHelm = (new HematiteArmor(HematiteArmorMaterial,HematiteHelmId , 0)).setUnlocalizedName("Hematite Helm");
		HematiteChest = (new HematiteArmor(HematiteArmorMaterial, HematiteChestId, 1)).setUnlocalizedName("Hematite Chest");
		HematiteLegs = (new HematiteArmor(HematiteArmorMaterial, HematiteLegsId, 2)).setUnlocalizedName("Hematite Legs");
		HematiteBoots = (new HematiteArmor(HematiteArmorMaterial, HematiteBootsId, 3)).setUnlocalizedName("Hematite Boots");
		//Pyrite Armor
		ArmorMaterial PyriteArmorMaterial = EnumHelper.addArmorMaterial("PyriteArmor", 240, new int[] {2, 4 , 2 , 2}, 10);
		PyriteHelm = (new PyriteArmor(PyriteArmorMaterial,PyriteHelmId , 0)).setUnlocalizedName("Pyrite Helm");
		PyriteChest = (new PyriteArmor(PyriteArmorMaterial, PyriteChestId, 1)).setUnlocalizedName("Pyrite Chest");
		PyriteLegs = (new PyriteArmor(PyriteArmorMaterial, PyriteLegsId, 2)).setUnlocalizedName("Pyrite Legs");
		PyriteBoots = (new PyriteArmor(PyriteArmorMaterial, PyriteBootsId, 3)).setUnlocalizedName("Pyrite Boots");

		//Gems	
		//Ocean
		AmmoniteGem = new WaterOreGem().setUnlocalizedName("AmmoniteGem").setTextureName("fishmod:Ammonite Gem"); 
		AquamarineGem = new WaterOreGem().setUnlocalizedName("AquamarineGem").setTextureName("fishmod:Aquamarine Gem");
		DiopsideGem = new WaterOreGem().setUnlocalizedName("DiopsideGem").setTextureName("fishmod:Diopside Gem");
		FluoriteGem = new WaterOreGem().setUnlocalizedName("FluoriteGem").setTextureName("fishmod:Fluorite Gem");
		TrilobiteGem = new WaterOreGem().setUnlocalizedName("TrilobiteGem").setTextureName("fishmod:Trilobite Gem");
		TurquoiseGem = new WaterOreGem().setUnlocalizedName("TurquoiseGem").setTextureName("fishmod:Turquoise Gem");
		//River
		BerylGem = new WaterOreGem().setUnlocalizedName("BerylGem").setTextureName("fishmod:Beryl Gem").setCreativeTab(fishmod);
		GarnetGem = new WaterOreGem().setUnlocalizedName("GarnetGem").setTextureName("fishmod:Garnet Gem").setCreativeTab(fishmod);
		JasperGem = new WaterOreGem().setUnlocalizedName("JasperGem").setTextureName("fishmod:Jasper Gem").setCreativeTab(fishmod);
		OpalGem = new WaterOreGem().setUnlocalizedName("OpalGem").setTextureName("fishmod:Opal Gem").setCreativeTab(fishmod);
		//Lake
		HematiteGem = new WaterOreGem().setUnlocalizedName("HematiteGem").setTextureName("fishmod:Hematite Gem");
		PyriteGem = new WaterOreGem().setUnlocalizedName("PyriteGem").setTextureName("fishmod:Pyrite Gem");
		//Fishing Rods
		//We are only using vanilla ores for fishing rods.
		WoodFishingRod = new WoodFishingRodClass(WoodFishingRodMaterial).setUnlocalizedName("WoodFishingRod").setTextureName("fishmod:Wood FishingRod").setCreativeTab(fishmod);
		StoneFishingRod = new VanillaOreFishingRod(StoneFishingRodMaterial).setUnlocalizedName("StoneFishingRod").setTextureName("fishmod:Stone FishingRod").setCreativeTab(fishmod);
		IronFishingRod = new VanillaOreFishingRod(IronFishingRodMaterial).setUnlocalizedName("IronFishingRod").setTextureName("fishmod:Iron FishingRod").setCreativeTab(fishmod);
		GoldFishingRod = new VanillaOreFishingRod(GoldFishingRodMaterial).setUnlocalizedName("GoldFishingRod").setTextureName("fishmod:Gold FishingRod").setCreativeTab(fishmod);
		DiamondFishingRod = new VanillaOreFishingRod(DiamondFishingRodMaterial).setUnlocalizedName("DiamondFishingRod").setTextureName("fishmod:Diamond FishingRod").setCreativeTab(fishmod);
		EmeraldFishingRod = new VanillaOreFishingRod(EmeraldFishingRodMaterial).setUnlocalizedName("EmeraldFishingRod").setTextureName("fishmod:Emerald FishingRod").setCreativeTab(fishmod);
		//SeaWeed
		SeaWeed = new Item().setUnlocalizedName("SeaWeed").setTextureName("fishmod:SeaWeed").setCreativeTab(fishmod);
//		SeaWeedHandle = new Item().setUnlocalizedName("SeaWeedHandle").setTextureName("fishmod:SeaWeed Handle").setCreativeTab(fishmod);

		//Food
		// ExampleFood = new ExampleFoodClass(hunger,saturation,isWolfFood);
		//Raw
		RawAnchovy = new RawFood(1,0,true).setTextureName("fishmod:Raw Anchovy").setUnlocalizedName("RawAnchovy").setCreativeTab(fishmod);
		RawCod = new RawFood(1,0,true).setTextureName("fishmod:Raw Cod").setUnlocalizedName("RawCod").setCreativeTab(fishmod);
		RawHaddock = new RawFood(1,0,true).setTextureName("fishmod:Raw Haddock").setUnlocalizedName("RawHaddok").setCreativeTab(fishmod);
		RawHerring = new RawFood(1,0,true).setTextureName("fishmod:Raw Herring").setUnlocalizedName("RawHerring").setCreativeTab(fishmod);
		RawMullet = new RawFood(1,0,true).setTextureName("fishmod:Raw Mullet").setUnlocalizedName("RawMullet").setCreativeTab(fishmod);
		RawPlaice = new RawFood(1,0,true).setTextureName("fishmod:Raw Plaice").setUnlocalizedName("RawPlaice").setCreativeTab(fishmod);
		RawSalmon = new RawFood(1,0,true).setTextureName("fishmod:Raw Salmon").setUnlocalizedName("RawSalmon").setCreativeTab(fishmod);
		RawSeaBass = new RawFood(1,0,true).setTextureName("fishmod:Raw SeaBass").setUnlocalizedName("RawSeaBass").setCreativeTab(fishmod);
		RawSharkFin = new RawSharkFoodClass(1,0,true).setTextureName("fishmod:Raw SharkFin").setUnlocalizedName("RawSharkFin").setCreativeTab(fishmod);
		RawSwordfish = new RawFood(1,0,true).setTextureName("fishmod:Raw Swordfish").setUnlocalizedName("RawSwordFish").setCreativeTab(fishmod);
		RawTrout = new RawFood(1,0,true).setTextureName("fishmod:Raw Trout").setUnlocalizedName("RawTrout").setCreativeTab(fishmod);
		RawTuna = new RawFood(1,0,true).setTextureName("fishmod:Raw Tuna").setUnlocalizedName("RawTuna").setCreativeTab(fishmod);
		//Cooked
		CookedAnchovy = new CookedFood(2,0,false).setTextureName("fishmod:Cooked Anchovy").setUnlocalizedName("CookedAnchovy").setCreativeTab(fishmod);
		CookedCod = new CookedFood(6,0,false).setTextureName("fishmod:Cooked Cod").setUnlocalizedName("CookedCod").setCreativeTab(fishmod);
		CookedHaddock = new CookedFood(5,0,false).setTextureName("fishmod:Cooked Haddock").setUnlocalizedName("CookedHaddock").setCreativeTab(fishmod);
		CookedHerring = new CookedFood(4,0,false).setTextureName("fishmod:Cooked Herring").setUnlocalizedName("CookedHerring").setCreativeTab(fishmod);
		CookedMullet = new CookedFood(5,0,false).setTextureName("fishmod:Cooked Mullet").setUnlocalizedName("CookedMullet").setCreativeTab(fishmod);
		CookedPlaice = new CookedFood(6,0,false).setTextureName("fishmod:Cooked Plaice").setUnlocalizedName("CookedPlaice").setCreativeTab(fishmod);
		CookedSalmon = new CookedFood(3,0,false).setTextureName("fishmod:Cooked Salmon").setUnlocalizedName("CookedSalmon").setCreativeTab(fishmod);
		CookedSeaBass = new CookedFood(2,0,false).setTextureName("fishmod:Cooked SeaBass").setUnlocalizedName("CookedSeaBass").setCreativeTab(fishmod);
		CookedSharkFin = new CookedSharkClass(8,0,false).setTextureName("fishmod:Cooked SharkFin").setUnlocalizedName("CookedSharkFin").setCreativeTab(fishmod);
		CookedSwordFish = new CookedSwordFishClass(7,0,false).setTextureName("fishmod:Cooked Swordfish").setUnlocalizedName("CookedSwordFish").setCreativeTab(fishmod);
		CookedTrout = new CookedFood(6,0,false).setTextureName("fishmod:Cooked Trout").setUnlocalizedName("CookedTrout").setCreativeTab(fishmod);
		CookedTuna = new CookedFood(3,0,false).setTextureName("fishmod:Cooked Tuna").setUnlocalizedName("CookedTuna").setCreativeTab(fishmod);
		//Other Fish Drops/Items
		SharkTooth = new SharkToothClass().setUnlocalizedName("SharkTooth").setTextureName("fishmod:Shark Tooth").setCreativeTab(fishmod);
		PurifiedWater = new Item().setUnlocalizedName("PurifiedWater").setTextureName("fishmod:Purified Water").setCreativeTab(fishmod);

		//Machines
		//Furnace
		//Ammonite
		//        AmmoniteFurnace_Idle = new AmmoniteFurnaceClass(false).setBlockName("AmmoniteFurnace_Idle").setCreativeTab(fishmod);
		//        AmmoniteFurnace_Active = new AmmoniteFurnaceClass(true).setBlockName("AmmoniteFurnace_Active");
		//Aquamarine

		//Blocks
		AmmoniteBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Ammonite Block").setBlockName("Ammonite Block").setCreativeTab(fishmod);
		AquamarineBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Aquamarine Block").setBlockName("Aquamarine Block").setCreativeTab(fishmod);
		DiopsideBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Diopside Block").setBlockName("Diopside Block").setCreativeTab(fishmod);
		FluoriteBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Fluorite Block").setBlockName("Fluorite Block").setCreativeTab(fishmod);
		TrilobiteBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Trilobite Block").setBlockName("Trilobite Block").setCreativeTab(fishmod);
		TurquoiseBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Turquoise Block").setBlockName("Turquoise Block").setCreativeTab(fishmod);

		BerylBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Beryl Block").setBlockName("Beryl Block").setCreativeTab(fishmod);
		GarnetBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Garnet Block").setBlockName("Garnet Block").setCreativeTab(fishmod);
		JasperBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Jasper Block").setBlockName("Jasper Block").setCreativeTab(fishmod);
		OpalBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Opal Block").setBlockName("Opal Block").setCreativeTab(fishmod);

		HematiteBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Hematite Block").setBlockName("Hematite Block").setCreativeTab(fishmod);
		PyriteBlock = new OreBlock(Material.rock).setBlockTextureName("fishmod:Pyrite Block").setBlockName("Pyrite Block").setCreativeTab(fishmod);

		//Bricks
		AmmoniteBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Ammonite Bricks").setBlockName("Ammonite Brick").setCreativeTab(fishmod);
		AquamarineBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Aquamarine Bricks").setBlockName("Aquamarine Brick").setCreativeTab(fishmod);
		DiopsideBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Diopside Bricks").setBlockName("Diopside Brick").setCreativeTab(fishmod);
		FluoriteBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Fluorite Bricks").setBlockName("Fluorite Brick").setCreativeTab(fishmod);
		TrilobiteBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Trilobite Bricks").setBlockName("Trilobite Brick").setCreativeTab(fishmod);
		TurquoiseBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Turquoise Bricks").setBlockName("Turquoise Brick").setCreativeTab(fishmod);

		BerylBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Beryl Bricks").setBlockName("Beryl Brick").setCreativeTab(fishmod);
		GarnetBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Garnet Bricks").setBlockName("Garnet Brick").setCreativeTab(fishmod);
		JasperBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Jasper Bricks").setBlockName("Jasper Brick").setCreativeTab(fishmod);
		OpalBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Opal Bricks").setBlockName("Opal Brick").setCreativeTab(fishmod);

		HematiteBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Hematite Bricks").setBlockName("Hematite Brick").setCreativeTab(fishmod);
		PyriteBrick = new OreBrick(Material.rock).setBlockTextureName("fishmod:Pyrite Bricks").setBlockName("Pyrite Brick").setCreativeTab(fishmod);

		//Decoratives
		AmmoniteDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Ammonite Decorative").setBlockName("Ammonite Decorative").setCreativeTab(fishmod);

		AquamarineDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Aquamarine Decorative").setBlockName("Aquamarine Decorative").setCreativeTab(fishmod);
		DiopsideDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Diopside Decorative").setBlockName("Diopside Decorative").setCreativeTab(fishmod);
		FluoriteDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Fluorite Decorative").setBlockName("Fluorite Decorative").setCreativeTab(fishmod);
		TrilobiteDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Trilobite Decorative").setBlockName("Trilobite Decorative").setCreativeTab(fishmod);
		TurquoiseDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Turquoise Decorative").setBlockName("Turquoise Decorative").setCreativeTab(fishmod);

		BerylDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Beryl Decorative").setBlockName("Beryl Decorative").setCreativeTab(fishmod);
		GarnetDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Garnet Decorative").setBlockName("Garnet Decorative").setCreativeTab(fishmod);
		JasperDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Jasper Decorative").setBlockName("Jasper Decorative").setCreativeTab(fishmod);
		OpalDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Opal Decorative").setBlockName("Opal Decorative").setCreativeTab(fishmod);

		HematiteDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Hematite Decorative").setBlockName("Hematite Decorative").setCreativeTab(fishmod);
		PyriteDecorative = new OreDecorative(Material.rock).setBlockTextureName("fishmod:Pyrite Decorative").setBlockName("Pyrite Decorative").setCreativeTab(fishmod);

		//Pillars
		AmmonitePillar = new AmmonitePillarClass(Material.rock).setBlockName("Ammonite Pillar").setCreativeTab(fishmod);
		AquamarinePillar = new AquamarinePillarClass(Material.rock).setBlockName("Aquamarine Pillar").setCreativeTab(fishmod);
		DiopsidePillar = new DiopsidePillarClass(Material.rock).setBlockName("Diopside Pillar").setCreativeTab(fishmod);
		FluoritePillar = new FluoritePillarClass(Material.rock).setBlockName("Fluorite Pillar").setCreativeTab(fishmod);
		TrilobitePillar = new TrilobitePillarClass(Material.rock).setBlockName("Trilobite Pillar").setCreativeTab(fishmod);
		TurquoisePillar = new TurquoisePillarClass(Material.rock).setBlockName("Turquoise Pillar").setCreativeTab(fishmod);

		BerylPillar = new BerylPillarClass(Material.rock).setBlockName("Beryl Pillar").setCreativeTab(fishmod);
		GarnetPillar = new GarnetPillarClass(Material.rock).setBlockName("Garnet Pillar").setCreativeTab(fishmod);
		JasperPillar = new JasperPillarClass(Material.rock).setBlockName("Jasper Pillar").setCreativeTab(fishmod);
		OpalPillar = new OpalPillarClass(Material.rock).setBlockName("Opal Pillar").setCreativeTab(fishmod);
		HematitePillar = new HematitePillarClass(Material.rock).setBlockName("Hematite Pillar").setCreativeTab(fishmod);
		PyritePillar = new PyritePillarClass(Material.rock).setBlockName("Pyrite Pillar").setCreativeTab(fishmod);


		//Equipment
		//        //Shark Armor
		//        ArmorMaterial SharkArmorMaterial = EnumHelper.addArmorMaterial("SharkArmor", 240, new int[] {2, 4 , 2 , 2}, 10);
		//        int SharkArmorInt = proxy.addArmor("SharkArmor");
		//        SharkHelm = (new SharkArmor(SharkArmorMaterial,SharkArmorInt , 0)).setUnlocalizedName("Shark Helm");
		//        SharkChest = (new SharkArmor(SharkArmorMaterial, SharkArmorInt, 1)).setUnlocalizedName("Shark Chest");
		//        SharkLegs = (new SharkArmor(SharkArmorMaterial, SharkArmorInt, 2)).setUnlocalizedName("Shark Legs");
		//        SharkBoots = (new SharkArmor(SharkArmorMaterial, SharkArmorInt, 3)).setUnlocalizedName("Shark Boots");

		//Scuba
		ArmorMaterial ScubaMaterial = EnumHelper.addArmorMaterial("ScubaArmor",240,new int[] {2,4,2,2}, 10);



	}
	@EventHandler
	public void Init(FMLInitializationEvent event)
	{
		/* REGISTRATION */
		//Water Ores
		GameRegistry.registerBlock(AmmoniteOre, "AmmoniteOre");
		GameRegistry.registerBlock(AquamarineOre, "AquamarineOre");
		GameRegistry.registerBlock(DiopsideOre,"DiopsideOre");
		GameRegistry.registerBlock(FluoriteOre,"FluoriteOre");
		GameRegistry.registerBlock(TrilobiteOre,"TrilobiteOre");
		GameRegistry.registerBlock(TurquoiseOre,"TurquoiseOre");

		GameRegistry.registerBlock(BerylOre,"BerylOre");
		GameRegistry.registerBlock(GarnetOre,"GarnetOre");
		GameRegistry.registerBlock(JasperOre,"JasperOre");
		GameRegistry.registerBlock(OpalOre,"OpalOre");

		GameRegistry.registerBlock(HematiteOre,"HematiteOre");
		GameRegistry.registerBlock(PyriteOre,"PyriteOre");
		//Tools
		//Ammonite
		GameRegistry.registerItem(AmmoniteAxe, "AmmoniteAxe");
		GameRegistry.registerItem(AmmoniteHoe, "AmmoniteHoe");
		GameRegistry.registerItem(AmmonitePickaxe, "AmmonitePickaxe");
		GameRegistry.registerItem(AmmoniteShovel, "AmmoniteShovel");
		GameRegistry.registerItem(AmmoniteSword, "AmmoniteSword");
		//Aquamarine
		GameRegistry.registerItem(AquamarineAxe, "AquamarineAxe");
		GameRegistry.registerItem(AquamarineHoe, "AquamarineHoe");
		GameRegistry.registerItem(AquamarinePickaxe, "AquamarinePickaxe");
		GameRegistry.registerItem(AquamarineShovel, "AquamarineShovel");
		GameRegistry.registerItem(AquamarineSword, "AquamarineSword");
		//Diopside
		GameRegistry.registerItem(DiopsideAxe, "DiopsideAxe");
		GameRegistry.registerItem(DiopsideHoe, "DiopsideHoe");
		GameRegistry.registerItem(DiopsidePickaxe, "DiopsidePickaxe");
		GameRegistry.registerItem(DiopsideShovel, "DiopsideShovel");
		GameRegistry.registerItem(DiopsideSword, "DiopsideSword");
		//Fluorite
		GameRegistry.registerItem(FluoriteAxe, "FluoriteAxe");
		GameRegistry.registerItem(FluoriteHoe, "FluoriteHoe");
		GameRegistry.registerItem(FluoritePickaxe, "FluoritePickaxe");
		GameRegistry.registerItem(FluoriteShovel, "FluoriteShovel");
		GameRegistry.registerItem(FluoriteSword, "FluoriteSword");
		//Trilobite
		GameRegistry.registerItem(TrilobiteAxe, "TrilobiteAxe");
		GameRegistry.registerItem(TrilobiteHoe, "TrilobiteHoe");
		GameRegistry.registerItem(TrilobitePickaxe, "TrilobitePickaxe");
		GameRegistry.registerItem(TrilobiteShovel, "TrilobiteShovel");
		GameRegistry.registerItem(TrilobiteSword, "TrilobiteSword");
		//Turquoise
		GameRegistry.registerItem(TurquoiseAxe, "TurquoiseAxe");
		GameRegistry.registerItem(TurquoiseHoe, "TurquoiseHoe");
		GameRegistry.registerItem(TurquoisePickaxe, "TurquoisePickaxe");
		GameRegistry.registerItem(TurquoiseShovel, "TurquoiseShovel");
		GameRegistry.registerItem(TurquoiseSword, "TurquoiseSword");
		//Beryl
		GameRegistry.registerItem(BerylAxe, "BerylAxe");
		GameRegistry.registerItem(BerylHoe, "BerylHoe");
		GameRegistry.registerItem(BerylPickaxe, "BerylPickaxe");
		GameRegistry.registerItem(BerylShovel, "BerylShovel");
		GameRegistry.registerItem(BerylSword, "BerylSword");
		//Garnet
		GameRegistry.registerItem(GarnetAxe, "GarnetAxe");
		GameRegistry.registerItem(GarnetHoe, "GarnetHoe");
		GameRegistry.registerItem(GarnetPickaxe, "GarnetPickaxe");
		GameRegistry.registerItem(GarnetShovel, "GarnetShovel");
		GameRegistry.registerItem(GarnetSword, "GarnetSword");
		//Jasper
		GameRegistry.registerItem(JasperAxe, "JasperAxe");
		GameRegistry.registerItem(JasperHoe, "JasperHoe");
		GameRegistry.registerItem(JasperPickaxe, "JasperPickaxe");
		GameRegistry.registerItem(JasperShovel, "JasperShovel");
		GameRegistry.registerItem(JasperSword, "JasperSword");
		//Opal
		GameRegistry.registerItem(OpalAxe, "OpalAxe");
		GameRegistry.registerItem(OpalHoe, "OpalHoe");
		GameRegistry.registerItem(OpalPickaxe, "OpalPickaxe");
		GameRegistry.registerItem(OpalShovel, "OpalShovel");
		GameRegistry.registerItem(OpalSword, "OpalSword");
		//Hematite
		GameRegistry.registerItem(HematiteAxe, "HematiteAxe");
		GameRegistry.registerItem(HematiteHoe, "HematiteHoe");
		GameRegistry.registerItem(HematitePickaxe, "HematitePickaxe");
		GameRegistry.registerItem(HematiteShovel, "HematiteShovel");
		GameRegistry.registerItem(HematiteSword, "HematiteSword");
		//Pyrite
		GameRegistry.registerItem(PyriteAxe, "PyriteAxe");
		GameRegistry.registerItem(PyriteHoe, "PyriteHoe");
		GameRegistry.registerItem(PyritePickaxe, "PyritePickaxe");
		GameRegistry.registerItem(PyriteShovel, "PyriteShovel");
		GameRegistry.registerItem(PyriteSword, "PyriteSword");
		//Sharks
		GameRegistry.registerItem(SharkAxe,"SharkAxe");
		GameRegistry.registerItem(SharkHoe,"SharkHoe");
		GameRegistry.registerItem(SharkPickaxe,"SharkPickaxe");
		GameRegistry.registerItem(SharkShovel,"SharkShovel");
		GameRegistry.registerItem(SharkSword,"SharkSword");
		//Coral
		GameRegistry.registerItem(CoralAxe, "CoralAxe");
		GameRegistry.registerItem(CoralHoe, "CoralHoe");
		GameRegistry.registerItem(CoralPickaxe, "CoralPickaxe");
		GameRegistry.registerItem(CoralShovel, "CoralShovel");
		GameRegistry.registerItem(CoralSword, "CoralSword");

		//Armor
		//Ammonite
		GameRegistry.registerItem(AmmoniteHelm, "AmmoniteHelm");
		GameRegistry.registerItem(AmmoniteChest,"AmmoniteChest");
		GameRegistry.registerItem(AmmoniteLegs,"AmmoniteLegs");
		GameRegistry.registerItem(AmmoniteBoots,"AmmoniteBoots");
		//Aquamarine
		GameRegistry.registerItem(AquamarineHelm, "AquamarineHelm");
		GameRegistry.registerItem(AquamarineChest,"AquamarineChest");
		GameRegistry.registerItem(AquamarineLegs,"AquamarineLegs");
		GameRegistry.registerItem(AquamarineBoots,"AquamarineBoots");
		//Diopside
		GameRegistry.registerItem(DiopsideHelm, "DiopsideHelm");
		GameRegistry.registerItem(DiopsideChest,"DiopsideChest");
		GameRegistry.registerItem(DiopsideLegs,"DiopsideLegs");
		GameRegistry.registerItem(DiopsideBoots,"DiopsideBoots");
		//Fluorite
		GameRegistry.registerItem(FluoriteHelm, "FluoriteHelm");
		GameRegistry.registerItem(FluoriteChest,"FluoriteChest");
		GameRegistry.registerItem(FluoriteLegs,"FluoriteLegs");
		GameRegistry.registerItem(FluoriteBoots,"FluoriteBoots");
		//Trilobite
		GameRegistry.registerItem(TrilobiteHelm, "TrilobiteHelm");
		GameRegistry.registerItem(TrilobiteChest,"TrilobiteChest");
		GameRegistry.registerItem(TrilobiteLegs,"TrilobiteLegs");
		GameRegistry.registerItem(TrilobiteBoots,"TrilobiteBoots");
		//Turquoise
		GameRegistry.registerItem(TurquoiseHelm, "TurquoiseHelm");
		GameRegistry.registerItem(TurquoiseChest,"TurquoiseChest");
		GameRegistry.registerItem(TurquoiseLegs,"TurquoiseLegs");
		GameRegistry.registerItem(TurquoiseBoots,"TurquoiseBoots");
		//Shark
		GameRegistry.registerItem(SharkHelm,"SharkHelm");
		GameRegistry.registerItem(SharkChest,"SharkChest");
		GameRegistry.registerItem(SharkLegs,"SharkLegs");
		GameRegistry.registerItem(SharkBoots,"SharkBoots");
		//Beryl
		GameRegistry.registerItem(BerylHelm,"BerylHelm");
		GameRegistry.registerItem(BerylChest,"BerylChest");
		GameRegistry.registerItem(BerylLegs,"BerylLegs");
		GameRegistry.registerItem(BerylBoots,"BerylBoots");
		//Garnet
		GameRegistry.registerItem(GarnetHelm,"GarnetHelm");
		GameRegistry.registerItem(GarnetChest,"GarnetChest");
		GameRegistry.registerItem(GarnetLegs,"GarnetLegs");
		GameRegistry.registerItem(GarnetBoots,"GarnetBoots");
		//Jasper
		GameRegistry.registerItem(JasperHelm,"JasperHelm");
		GameRegistry.registerItem(JasperChest,"JasperChest");
		GameRegistry.registerItem(JasperLegs,"JasperLegs");
		GameRegistry.registerItem(JasperBoots,"JasperBoots");
		//Opal
		GameRegistry.registerItem(OpalHelm,"OpalHelm");
		GameRegistry.registerItem(OpalChest,"OpalChest");
		GameRegistry.registerItem(OpalLegs,"OpalLegs");
		GameRegistry.registerItem(OpalBoots,"OpalBoots");
		//Hematite
		GameRegistry.registerItem(HematiteHelm,"HematiteHelm");
		GameRegistry.registerItem(HematiteChest,"HematiteChest");
		GameRegistry.registerItem(HematiteLegs,"HematiteLegs");
		GameRegistry.registerItem(HematiteBoots,"HematiteBoots");
		//Pyrite
		GameRegistry.registerItem(PyriteHelm,"PyriteHelm");
		GameRegistry.registerItem(PyriteChest,"PyriteChest");
		GameRegistry.registerItem(PyriteLegs,"PyriteLegs");
		GameRegistry.registerItem(PyriteBoots,"PyriteBoots");
		//Gems
		GameRegistry.registerItem(AmmoniteGem,"AmmoniteGem");
		GameRegistry.registerItem(AquamarineGem,"AquamarineGem");
		GameRegistry.registerItem(DiopsideGem, "DiopsideGem");
		GameRegistry.registerItem(FluoriteGem,"FluoriteGem");
		GameRegistry.registerItem(TrilobiteGem,"TrilobiteGem");
		GameRegistry.registerItem(TurquoiseGem, "TurquoiseGem");
		GameRegistry.registerItem(BerylGem,"BerylGem");
		GameRegistry.registerItem(GarnetGem,"GarnetGem");
		GameRegistry.registerItem(JasperGem,"JasperGem");
		GameRegistry.registerItem(OpalGem,"OpalGem");
		GameRegistry.registerItem(HematiteGem,"HematiteGem");
		GameRegistry.registerItem(PyriteGem,"PyriteGem");
		//Fishing Rods
		GameRegistry.registerItem(WoodFishingRod,"WoodFishingRod");
		GameRegistry.registerItem(StoneFishingRod,"StoneFishingRod");
		GameRegistry.registerItem(IronFishingRod,"IronFishingRod");
		GameRegistry.registerItem(GoldFishingRod,"GoldFishingRod");
		GameRegistry.registerItem(DiamondFishingRod,"DiamondFishingRod");
		GameRegistry.registerItem(EmeraldFishingRod,"EmeraldFishingRod");
		//Raw Food
		GameRegistry.registerItem(RawAnchovy,"RawAnchovy");
		GameRegistry.registerItem(RawCod,"RawCod");
		GameRegistry.registerItem(RawHaddock,"RawHaddok");
		GameRegistry.registerItem(RawHerring,"RawHerring");
		GameRegistry.registerItem(RawMullet,"RawMullet");
		GameRegistry.registerItem(RawPlaice,"RawPlaice");
		GameRegistry.registerItem(RawSalmon,"RawSalmon");
		GameRegistry.registerItem(RawSeaBass,"RawSeaBass");
		GameRegistry.registerItem(RawSharkFin,"RawSharkFin");
		GameRegistry.registerItem(RawSwordfish,"RawSwordfish");
		GameRegistry.registerItem(RawTrout,"RawTrout");
		GameRegistry.registerItem(RawTuna,"RawTuna");
		//Cooked Food
		GameRegistry.registerItem(CookedAnchovy,"CookedAnchovy");
		GameRegistry.registerItem(CookedCod,"CookedCod");
		GameRegistry.registerItem(CookedHaddock,"CookedHaddok");
		GameRegistry.registerItem(CookedHerring,"CookedHerring");
		GameRegistry.registerItem(CookedMullet,"CookedMullet");
		GameRegistry.registerItem(CookedPlaice,"CookedPlaice");
		GameRegistry.registerItem(CookedSalmon,"CookedSalmon");
		GameRegistry.registerItem(CookedSeaBass,"CookedSeaBass");
		GameRegistry.registerItem(CookedSharkFin,"CookedSharkFin");
		GameRegistry.registerItem(CookedSwordFish,"CookedSwordFish");
		GameRegistry.registerItem(CookedTrout,"CookedTrout");
		GameRegistry.registerItem(CookedTuna,"CookedTuna");
		//Others
		GameRegistry.registerItem(SharkTooth,"SharkTooth");
		GameRegistry.registerItem(PurifiedWater,"PurifiedWater");
		//SeaWeed
		GameRegistry.registerItem(SeaWeed,"SeaWeed");
//		GameRegistry.registerItem(SeaWeedHandle,"SeaWeedHandle");
		//Machines
		//Furnace
		//Ammonite
		//  GameRegistry.registerBlock(AmmoniteFurnace_Idle,"AmmoniteFurnace_Idle");
		// GameRegistry.registerBlock(AmmoniteFurnace_Active,"AmmoniteFurnace_Active");
		//Blocks
		GameRegistry.registerBlock(AmmoniteBlock,"AmmoniteBlock");
		GameRegistry.registerBlock(AquamarineBlock,"AquamarineBlock");
		GameRegistry.registerBlock(DiopsideBlock,"DiopsideBlock");
		GameRegistry.registerBlock(FluoriteBlock,"FluoriteBlock");
		GameRegistry.registerBlock(TrilobiteBlock,"TrilobiteBlock");
		GameRegistry.registerBlock(TurquoiseBlock,"TurquoiseBlock");

		GameRegistry.registerBlock(BerylBlock,"BerylBlock");
		GameRegistry.registerBlock(GarnetBlock,"GarnetBlock");
		GameRegistry.registerBlock(JasperBlock,"JasperBlock");
		GameRegistry.registerBlock(OpalBlock,"OpalBlock");

		GameRegistry.registerBlock(HematiteBlock,"HematiteBlock");
		GameRegistry.registerBlock(PyriteBlock,"PyriteBlock");

		//Bricks
		GameRegistry.registerBlock(AmmoniteBrick,"AmmoniteBrick");
		GameRegistry.registerBlock(AquamarineBrick,"AquamarineBrick");
		GameRegistry.registerBlock(DiopsideBrick,"DiopsideBrick");
		GameRegistry.registerBlock(FluoriteBrick,"FluoriteBrick");
		GameRegistry.registerBlock(TrilobiteBrick,"TrilobiteBrick");
		GameRegistry.registerBlock(TurquoiseBrick,"TurquoiseBrick");

		GameRegistry.registerBlock(BerylBrick,"BerylBrick");
		GameRegistry.registerBlock(GarnetBrick,"GarnetBrick");
		GameRegistry.registerBlock(JasperBrick,"JasperBrick");
		GameRegistry.registerBlock(OpalBrick,"OpalBrick");

		GameRegistry.registerBlock(HematiteBrick,"HematiteBrick");
		GameRegistry.registerBlock(PyriteBrick,"PyriteBrick");

		//Decoratives
		GameRegistry.registerBlock(AmmoniteDecorative,"AmmoniteDecorative");
		GameRegistry.registerBlock(AquamarineDecorative,"AquamarineDecorative");
		GameRegistry.registerBlock(DiopsideDecorative,"DiopsideDecorative");
		GameRegistry.registerBlock(FluoriteDecorative,"FluoriteDecorative");
		GameRegistry.registerBlock(TrilobiteDecorative,"TrilobiteDecorative");
		GameRegistry.registerBlock(TurquoiseDecorative,"TurquoiseDecorative");

		GameRegistry.registerBlock(BerylDecorative,"BerylDecorative");
		GameRegistry.registerBlock(GarnetDecorative,"GarnetDecorative");
		GameRegistry.registerBlock(JasperDecorative,"JasperDecorative");
		GameRegistry.registerBlock(OpalDecorative,"OpalDecorative");

		GameRegistry.registerBlock(HematiteDecorative,"HematiteDecorative");
		GameRegistry.registerBlock(PyriteDecorative,"PyriteDecorative");
		//Pillars
		GameRegistry.registerBlock(AmmonitePillar,"AmmonitePillar");
		GameRegistry.registerBlock(AquamarinePillar,"AquamarinePillar");
		GameRegistry.registerBlock(DiopsidePillar,"DiopsidePillar");
		GameRegistry.registerBlock(FluoritePillar,"FluoritePillar");
		GameRegistry.registerBlock(TrilobitePillar,"TrilobitePillar");
		GameRegistry.registerBlock(TurquoisePillar,"TurquoisePillar");

		GameRegistry.registerBlock(BerylPillar,"BerylPillar");
		GameRegistry.registerBlock(GarnetPillar,"GarnetPillar");
		GameRegistry.registerBlock(JasperPillar,"JasperPillar");
		GameRegistry.registerBlock(OpalPillar,"OpalPillar");

		GameRegistry.registerBlock(HematitePillar,"HematitePillar");
		GameRegistry.registerBlock(PyritePillar,"PyritePillar");

		//Entities
		EntityHandler.registerEntities(TunaFish.class, "Tuna");
        EntityHandler.registerEntities(CodFish.class, "Cod");
        EntityHandler.registerEntities(SalmonFish.class, "Salmon");

		/* FURNACE RECIPES */
		GameRegistry.addSmelting(Items.water_bucket, new ItemStack(PurifiedWater),0.5F);
		//Gems
		GameRegistry.addSmelting(AmmoniteOre,new ItemStack(AmmoniteGem),0.5F);
		GameRegistry.addSmelting(AquamarineOre,new ItemStack(AquamarineGem),0.5F);
		GameRegistry.addSmelting(DiopsideOre,new ItemStack(DiopsideGem),0.5F);
		GameRegistry.addSmelting(FluoriteOre,new ItemStack(FluoriteGem),0.5F);
		GameRegistry.addSmelting(TrilobiteOre,new ItemStack(TrilobiteGem),0.5F);
		GameRegistry.addSmelting(TurquoiseOre,new ItemStack(TurquoiseGem),0.5F);

		GameRegistry.addSmelting(BerylOre,new ItemStack(BerylGem),0.5F);
		GameRegistry.addSmelting(GarnetOre,new ItemStack(GarnetGem),0.5F);
		GameRegistry.addSmelting(JasperOre,new ItemStack(JasperGem),0.5F);
		GameRegistry.addSmelting(OpalOre,new ItemStack(OpalGem),0.5F);

		GameRegistry.addSmelting(HematiteOre,new ItemStack(HematiteGem),0.5F);
		GameRegistry.addSmelting(PyriteOre,new ItemStack(PyriteGem),0.5F);

		//Food
		GameRegistry.addSmelting(RawAnchovy,new ItemStack(CookedAnchovy), 0.5F);
		GameRegistry.addSmelting(RawCod, new ItemStack(CookedCod), 0.5F);
		GameRegistry.addSmelting(RawHaddock, new ItemStack(CookedHaddock), 0.5F);
		GameRegistry.addSmelting(RawHerring,new ItemStack(CookedHerring), 0.5F);
		GameRegistry.addSmelting(RawMullet,new ItemStack(CookedMullet), 0.5F);
		GameRegistry.addSmelting(RawPlaice, new ItemStack(CookedPlaice), 0.5F);
		GameRegistry.addSmelting(RawSalmon, new ItemStack(CookedSalmon), 0.5F);
		GameRegistry.addSmelting(RawSeaBass, new ItemStack(CookedSeaBass), 0.5F);
		GameRegistry.addSmelting(RawSharkFin, new ItemStack(CookedSharkFin), 0.5F);
		GameRegistry.addSmelting(RawSwordfish, new ItemStack(CookedSwordFish), 0.5F);
		GameRegistry.addSmelting(RawTrout, new ItemStack(CookedTrout), 0.5F);
		GameRegistry.addSmelting(RawTuna, new ItemStack(CookedTuna), 0.5F);

		/* CRAFTING RECIPES */
		//Tools
		//Ammonite
		GameRegistry.addRecipe(new ItemStack(AmmoniteAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', AmmoniteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', AmmoniteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AmmonitePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', AmmoniteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteShovel), new Object[]{
			" A ",
			" B ",
			" B ",
			'A', AmmoniteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', AmmoniteGem, 'B', Items.stick
		});
		//Aquamarine
		GameRegistry.addRecipe(new ItemStack(AquamarineAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', AquamarineGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', AquamarineGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AquamarinePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', AquamarineGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineShovel), new Object[]{
			" A ",
			" B ",
			" B ",
			'A', AquamarineGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', AquamarineGem, 'B', Items.stick
		});
		//Diopside
		GameRegistry.addRecipe(new ItemStack(DiopsideAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', DiopsideGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', DiopsideGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(DiopsidePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', DiopsideGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideShovel), new Object[]{
			" A ",
			" B ",
			" B ",
			'A', DiopsideGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', DiopsideGem, 'B', Items.stick
		});
		//Fluorite
		GameRegistry.addRecipe(new ItemStack(FluoriteAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', FluoriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', FluoriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(FluoritePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', FluoriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteShovel), new Object[]{
			" A ",
			" B ",
			" B ",
			'A', FluoriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', FluoriteGem, 'B', Items.stick
		});
		//Trilobite
		GameRegistry.addRecipe(new ItemStack(TrilobiteAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', TrilobiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', TrilobiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TrilobitePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', TrilobiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteShovel), new Object[]{
			" A ",
			" B ",
			" B ",
			'A', TrilobiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', TrilobiteGem, 'B', Items.stick
		});
		//Turquoise
		GameRegistry.addRecipe(new ItemStack(TurquoiseAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', TurquoiseGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', TurquoiseGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TurquoisePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', TurquoiseGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', TurquoiseGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', TurquoiseGem, 'B', Items.stick
		});
		//Beryl
		GameRegistry.addRecipe(new ItemStack(BerylAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', BerylGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(BerylHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', BerylGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(BerylPickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', BerylGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(BerylShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', BerylGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(BerylSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', BerylGem, 'B', Items.stick
		});
		//Garnet
		GameRegistry.addRecipe(new ItemStack(GarnetAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', GarnetGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(GarnetHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', GarnetGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(GarnetPickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', GarnetGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(GarnetShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', GarnetGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(GarnetSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', GarnetGem, 'B', Items.stick
		});
		//Jasper
		GameRegistry.addRecipe(new ItemStack(JasperAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', JasperGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(JasperHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', JasperGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(JasperPickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', JasperGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(JasperShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', JasperGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(JasperSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', JasperGem, 'B', Items.stick
		});
		//Opal
		GameRegistry.addRecipe(new ItemStack(OpalAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', OpalGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(OpalHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', OpalGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(OpalPickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', OpalGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(OpalShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', OpalGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(OpalSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', OpalGem, 'B', Items.stick
		});
		//Hematite
		GameRegistry.addRecipe(new ItemStack(HematiteAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', HematiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(HematiteHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', HematiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(HematitePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', HematiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(HematiteShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', HematiteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(HematiteSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', HematiteGem, 'B', Items.stick
		});
		//Pyrite
		GameRegistry.addRecipe(new ItemStack(PyriteAxe), new Object[]{
			" AA",
			" BA",
			" B ",
			'A', PyriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(PyriteHoe), new Object[]{
			" AA",
			" B ",
			" B ",
			'A', PyriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(PyritePickaxe), new Object[]{
			"AAA",
			" B ",
			" B ",
			'A', PyriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(PyriteShovel), new Object[]{
			" A ",
			" B  ",
			" B ",
			'A', PyriteGem, 'B', Items.stick
		});
		GameRegistry.addRecipe(new ItemStack(PyriteSword), new Object[]{
			" A ",
			" A ",
			" B ",
			'A', PyriteGem, 'B', Items.stick
		});
        //Seaweed and Coral
        GameRegistry.addRecipe(new ItemStack(CoralAxe), new Object[]{
                "BB ",
                "BA ",
                " A ",
                'A', SeaWeed, 'B', CoralBlock
        });
        GameRegistry.addRecipe(new ItemStack(CoralHoe), new Object[] {
                "BB ",
                " A ",
                " A ",
                'A', SeaWeed, 'B', CoralBlock
        });
        GameRegistry.addRecipe(new ItemStack(CoralPickaxe), new Object[]{
            "BBB",
            " A ",
            " A ",
            'A', SeaWeed, 'B', CoralBlock
        });
        GameRegistry.addRecipe(new ItemStack(CoralShovel), new Object[]{
            " B ",
            " A ",
            " A ",
            'A', SeaWeed, 'B', CoralBlock
        });
        GameRegistry.addRecipe(new ItemStack(CoralSword), new Object[]{
                " B ",
                " B ",
                " A ",
                'A', SeaWeed, 'B', CoralBlock
        });
		//Armor
		//Ammonite Armor
		GameRegistry.addRecipe(new ItemStack(AmmoniteHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', AmmoniteGem,
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', AmmoniteGem,
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', AmmoniteGem,
		});
		GameRegistry.addRecipe(new ItemStack(AmmoniteBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', AmmoniteGem,
		});
		//Aquamarine Armor
		GameRegistry.addRecipe(new ItemStack(AquamarineHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', AquamarineGem,
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', AquamarineGem,
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', AquamarineGem,
		});
		GameRegistry.addRecipe(new ItemStack(AquamarineBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', AquamarineGem,
		});
		//Diopside Armor
		GameRegistry.addRecipe(new ItemStack(DiopsideHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', DiopsideGem,
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', DiopsideGem,
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', DiopsideGem,
		});
		GameRegistry.addRecipe(new ItemStack(DiopsideBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', DiopsideGem,
		});
		//Fluorite Armor
		GameRegistry.addRecipe(new ItemStack(FluoriteHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', FluoriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', FluoriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', FluoriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(FluoriteBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', FluoriteGem,
		});
		//Trilobite Armor
		GameRegistry.addRecipe(new ItemStack(TrilobiteHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', TrilobiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', TrilobiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', TrilobiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(TrilobiteBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', TrilobiteGem,
		});
		//Turquoise Armor
		GameRegistry.addRecipe(new ItemStack(TurquoiseHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', TurquoiseGem,
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', TurquoiseGem,
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', TurquoiseGem,
		});
		GameRegistry.addRecipe(new ItemStack(TurquoiseBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', TurquoiseGem,
		});
		//Beryl Armor
		GameRegistry.addRecipe(new ItemStack(BerylHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', BerylGem,
		});
		GameRegistry.addRecipe(new ItemStack(BerylChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', BerylGem,
		});
		GameRegistry.addRecipe(new ItemStack(BerylLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', BerylGem,
		});
		GameRegistry.addRecipe(new ItemStack(BerylBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', BerylGem,
		});
		//Garnet Armor
		GameRegistry.addRecipe(new ItemStack(GarnetHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', GarnetGem,
		});
		GameRegistry.addRecipe(new ItemStack(GarnetChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', GarnetGem,
		});
		GameRegistry.addRecipe(new ItemStack(GarnetLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', GarnetGem,
		});
		GameRegistry.addRecipe(new ItemStack(GarnetBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', GarnetGem,
		});
		//Jasper Armor
		GameRegistry.addRecipe(new ItemStack(JasperHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', JasperGem,
		});
		GameRegistry.addRecipe(new ItemStack(JasperChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', JasperGem,
		});
		GameRegistry.addRecipe(new ItemStack(JasperLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', JasperGem,
		});
		GameRegistry.addRecipe(new ItemStack(JasperBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', JasperGem,
		});
		//Opal Armor
		GameRegistry.addRecipe(new ItemStack(OpalHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', OpalGem,
		});
		GameRegistry.addRecipe(new ItemStack(OpalChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', OpalGem,
		});
		GameRegistry.addRecipe(new ItemStack(OpalLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', OpalGem,
		});
		GameRegistry.addRecipe(new ItemStack(OpalBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', OpalGem,
		});
		//Hematite Armor
		GameRegistry.addRecipe(new ItemStack(HematiteHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', HematiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(HematiteChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', HematiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(HematiteLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', HematiteGem,
		});
		GameRegistry.addRecipe(new ItemStack(HematiteBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', HematiteGem,
		});
		//Pyrite Armor
		GameRegistry.addRecipe(new ItemStack(PyriteHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', PyriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(PyriteChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', PyriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(PyriteLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', PyriteGem,
		});
		GameRegistry.addRecipe(new ItemStack(PyriteBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', PyriteGem,
		});
		//Shark Armor
		GameRegistry.addRecipe(new ItemStack(SharkHelm), new Object[]{
			"AAA",
			"A A",
			"   ",
			'A', SharkTooth,
		});
		GameRegistry.addRecipe(new ItemStack(SharkChest), new Object[]{
			"A A",
			"AAA",
			"AAA",
			'A', SharkTooth,
		});
		GameRegistry.addRecipe(new ItemStack(SharkLegs), new Object[]{
			"AAA",
			"A A",
			"A A",
			'A', SharkTooth,
		});
		GameRegistry.addRecipe(new ItemStack(SharkBoots), new Object[]{
			"A A",
			"A A",
			"   ",
			'A', SharkTooth,
		});
		//Fishing Rods
		//Stone
		GameRegistry.addRecipe(new ItemStack(StoneFishingRod), new Object[] {
			" A ",
			"ABA",
			" A ",
			'A', Blocks.stone, 'B', Items.fishing_rod,
		});
		//Iron
		GameRegistry.addRecipe(new ItemStack(IronFishingRod), new Object[] {
			" A ",
			"ABA",
			" A ",
			'A', Items.iron_ingot, 'B', StoneFishingRod,
		});
		//Gold
		GameRegistry.addRecipe(new ItemStack(GoldFishingRod), new Object[] {
			" A ",
			"ABA",
			" A ",
			'A', Items.gold_ingot, 'B', IronFishingRod,
		});
		//Diamond
		GameRegistry.addRecipe(new ItemStack(DiamondFishingRod), new Object[] {
			" A ",
			"ABA",
			" A ",
			'A', Items.diamond, 'B' ,GoldFishingRod,
		});
		//Emerald
		GameRegistry.addRecipe(new ItemStack(DiamondFishingRod), new Object[] {
			" A ",
			"ABA",
			" A ",
			'A', Items.emerald, 'B' ,DiamondFishingRod,
		});

		//Blocks
		//Ammonite
		GameRegistry.addRecipe(new ItemStack(AmmoniteBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', AmmoniteGem});

		//Aquamarine
		GameRegistry.addRecipe(new ItemStack(AquamarineBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', AquamarineGem});
		//Diopside
		GameRegistry.addRecipe(new ItemStack(DiopsideBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', DiopsideGem});
		//Fluorite
		GameRegistry.addRecipe(new ItemStack(FluoriteBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', FluoriteGem});
		//Trilobite
		GameRegistry.addRecipe(new ItemStack(TrilobiteBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', TrilobiteGem});
		//Turquoise
		GameRegistry.addRecipe(new ItemStack(TurquoiseBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', TurquoiseGem});

		//Beryl
		GameRegistry.addRecipe(new ItemStack(BerylBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', BerylGem});
		//Garnet
		GameRegistry.addRecipe(new ItemStack(GarnetBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', GarnetGem});
		//Jasper
		GameRegistry.addRecipe(new ItemStack(JasperBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', JasperGem});
		//Opal
		GameRegistry.addRecipe(new ItemStack(OpalBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', OpalGem});


		//Hematite
		GameRegistry.addRecipe(new ItemStack(HematiteBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', HematiteGem});
		//Pyrite
		GameRegistry.addRecipe(new ItemStack(PyriteBlock), new Object[] {
			"AAA",
			"AAA",
			"AAA", 'A', PyriteGem});

		//Pillars
		//Ammonite
		GameRegistry.addRecipe(new ItemStack(AmmoniteBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', AmmoniteGem});
		//Aquamarine
		GameRegistry.addRecipe(new ItemStack(AquamarineBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', AquamarineGem});
		//Diopside
		GameRegistry.addRecipe(new ItemStack(DiopsideBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', DiopsideGem});
		//Fluorite
		GameRegistry.addRecipe(new ItemStack(FluoriteBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', FluoriteGem});
		//Trilobite
		GameRegistry.addRecipe(new ItemStack(TrilobiteBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', TrilobiteGem});
		//Turquoise
		GameRegistry.addRecipe(new ItemStack(TurquoiseBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', TurquoiseGem});

		//Beryl
		GameRegistry.addRecipe(new ItemStack(BerylBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', BerylGem});
		//Garnet
		GameRegistry.addRecipe(new ItemStack(GarnetBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', GarnetGem});
		//Jasper
		GameRegistry.addRecipe(new ItemStack(JasperBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', JasperGem});
		//Opal
		GameRegistry.addRecipe(new ItemStack(OpalBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', OpalGem});


		//Hematite
		GameRegistry.addRecipe(new ItemStack(HematiteBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', HematiteGem});
		//Pyrite
		GameRegistry.addRecipe(new ItemStack(PyriteBrick), new Object[] {
			"AA ",
			"AA ",
			"   ", 'A', PyriteGem});
		//Bricks

		//Ammonite
		GameRegistry.addRecipe(new ItemStack(AmmonitePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', AmmoniteGem});
		//Aquamarine
		GameRegistry.addRecipe(new ItemStack(AquamarinePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', AquamarineGem});
		//Diopside
		GameRegistry.addRecipe(new ItemStack(DiopsidePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', DiopsideGem});
		//Fluorite
		GameRegistry.addRecipe(new ItemStack(FluoritePillar), new Object[] {
			"A  ",
			"A  ",
			"A  ", 'A', FluoriteGem});
		//Trilobite
		GameRegistry.addRecipe(new ItemStack(TrilobitePillar), new Object[] {
			"A  ",
			"A  ",
			"A  ", 'A', TrilobiteGem});
		//Turquoise
		GameRegistry.addRecipe(new ItemStack(TurquoisePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', TurquoiseGem});

		//Beryl
		GameRegistry.addRecipe(new ItemStack(BerylPillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', BerylGem});
		//Garnet
		GameRegistry.addRecipe(new ItemStack(GarnetPillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', GarnetGem});
		//Jasper
		GameRegistry.addRecipe(new ItemStack(JasperPillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', JasperGem});
		//Opal
		GameRegistry.addRecipe(new ItemStack(OpalPillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', OpalGem});


		//Hematite
		GameRegistry.addRecipe(new ItemStack(HematitePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', HematiteGem});
		//Pyrite
		GameRegistry.addRecipe(new ItemStack(PyritePillar), new Object[] {
			"A  ",
			"A  ",
			"   ", 'A', PyriteGem});

		//Register World Generation
		//        GameRegistry.registerWorldGenerator(new WorldGenMyOres(), 0);

	}
	@EventHandler
	public void load (FMLInitializationEvent event)
	{ 	
		proxy.registerRenderThings();
	}

	@EventHandler
	public void PostInit(FMLPostInitializationEvent postEvent){

	}




}
