package com.z3t0zlabz.fishmod.models.armor;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class AmmoniteArmor extends ItemArmor{
		 public AmmoniteArmor(ArmorMaterial material, int id, int slot) {
		  super(material, id, slot);
		  this.setCreativeTab(FishMod.fishmod);
		  
		  //Icon Texture
		  	if (slot == 0) {
		  		this.setTextureName(References.MODID + ":Ammonite Helm"); //If Helmet
		  	}
		  	else if (slot == 1){
		  		this.setTextureName(References.MODID + ":Ammonite Chest");//If Chest
		  	}
		  	else if (slot == 2){
		  		this.setTextureName(References.MODID + ":Ammonite Legs"); //If Legs
		  	}
		  	else if (slot == 3){
		  		this.setTextureName(References.MODID + ":Ammonite Boots"); //If Boots
		  	}
		  
		 }
		 
		 @Override
		 public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
		 {

		  if(stack.getItem() == FishMod.AmmoniteHelm || stack.getItem() == FishMod.AmmoniteChest ||  stack.getItem() == FishMod.AmmoniteBoots ){

		   return References.MODID + ":textures/models/armor/AmmoniteArmor_layer_1.png";

		  }


		  else if(stack.getItem() == FishMod.AmmoniteLegs){
		   return References.MODID + ":textures/models/armor/AmmoniteArmor_layer_2.png";

		  }

		  else{
			  return null;
		  }
		  
		 }
		 
}

