package com.z3t0zlabz.fishmod.models.armor;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class GarnetArmor extends ItemArmor {
    public GarnetArmor(ArmorMaterial material, int id, int slot) {
        super(material, id, slot);
        this.setCreativeTab(FishMod.fishmod);

        //Icon Texture
        if (slot == 0) {
            this.setTextureName(References.MODID + ":Garnet Helm"); //If Helmet
        }
        else if (slot == 1){
            this.setTextureName(References.MODID + ":Garnet Chest");//If Chest
        }
        else if (slot == 2){
            this.setTextureName(References.MODID + ":Garnet Legs"); //If Legs
        }
        else if (slot == 3){
            this.setTextureName(References.MODID + ":Garnet Boots"); //If Boots
        }

    }

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
    {

        if(stack.getItem() == FishMod.GarnetHelm || stack.getItem() == FishMod.GarnetChest ||  stack.getItem() == FishMod.GarnetBoots ){

            return References.MODID + ":textures/models/armor/GarnetArmor_layer_1.png";

        }


        else if(stack.getItem() == FishMod.GarnetLegs){
            return References.MODID + ":textures/models/armor/GarnetArmor_layer_2.png";

        }

        else{
            return null;
        }

    }

}
