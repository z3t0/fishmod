package com.z3t0zlabz.fishmod.models.armor;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor.ArmorMaterial;

public class TrilobiteArmor extends ItemArmor {
	 public TrilobiteArmor(ArmorMaterial material, int id, int slot) {
		  super(material, id, slot);
		  this.setCreativeTab(FishMod.fishmod);
		  
		  //Icon Texture
		  	if (slot == 0) {
		  		this.setTextureName(References.MODID + ":Trilobite Helm"); //If Helmet
		  	}
		  	else if (slot == 1){
		  		this.setTextureName(References.MODID + ":Trilobite Chest");//If Chest
		  	}
		  	else if (slot == 2){
		  		this.setTextureName(References.MODID + ":Trilobite Legs"); //If Legs
		  	}
		  	else if (slot == 3){
		  		this.setTextureName(References.MODID + ":Trilobite Boots"); //If Boots
		  	}
		  
		 }
		 
		 @Override
		 public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
		 {

		  if(stack.getItem() == FishMod.TrilobiteHelm || stack.getItem() == FishMod.TrilobiteChest ||  stack.getItem() == FishMod.TrilobiteBoots ){

		   return References.MODID + ":textures/models/armor/TrilobiteArmor_layer_1.png";

		  }


		  else if(stack.getItem() == FishMod.TrilobiteLegs){
		   return References.MODID + ":textures/models/armor/TrilobiteArmor_layer_2.png";

		  }

		  else{
			  return null;
		  }
		  
		 }

}
