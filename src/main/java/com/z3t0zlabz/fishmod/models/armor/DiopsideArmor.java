package com.z3t0zlabz.fishmod.models.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor.ArmorMaterial;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;

public class DiopsideArmor extends ItemArmor{
		 public DiopsideArmor(ArmorMaterial material, int id, int slot) {
		  super(material, id, slot);
		  this.setCreativeTab(FishMod.fishmod);
		  
		  //Icon Texture
		  	if (slot == 0) {
		  		this.setTextureName(References.MODID + ":Diopside Helm"); //If Helmet
		  	}
		  	else if (slot == 1){
		  		this.setTextureName(References.MODID + ":Diopside Chest");//If Chest
		  	}
		  	else if (slot == 2){
		  		this.setTextureName(References.MODID + ":Diopside Legs"); //If Legs
		  	}
		  	else if (slot == 3){
		  		this.setTextureName(References.MODID + ":Diopside Boots"); //If Boots
		  	}
		  
		 }
		 
		 @Override
		 public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
		 {

		  if(stack.getItem() == FishMod.DiopsideHelm || stack.getItem() == FishMod.DiopsideChest ||  stack.getItem() == FishMod.DiopsideBoots ){

		   return References.MODID + ":textures/models/armor/DiopsideArmor_layer_1.png";

		  }


		  else if(stack.getItem() == FishMod.DiopsideLegs){
		   return References.MODID + ":textures/models/armor/DiopsideArmor_layer_2.png";

		  }

		  else{
			  return null;
		  }
		  
		 }

	}

