package com.z3t0zlabz.fishmod.models.armor;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class JasperArmor extends ItemArmor {
    public JasperArmor(ArmorMaterial material, int id, int slot) {
        super(material, id, slot);
        this.setCreativeTab(FishMod.fishmod);

        //Icon Texture
        if (slot == 0) {
            this.setTextureName(References.MODID + ":Jasper Helm"); //If Helmet
        }
        else if (slot == 1){
            this.setTextureName(References.MODID + ":Jasper Chest");//If Chest
        }
        else if (slot == 2){
            this.setTextureName(References.MODID + ":Jasper Legs"); //If Legs
        }
        else if (slot == 3){
            this.setTextureName(References.MODID + ":Jasper Boots"); //If Boots
        }

    }

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
    {

        if(stack.getItem() == FishMod.JasperHelm || stack.getItem() == FishMod.JasperChest ||  stack.getItem() == FishMod.JasperBoots ){

            return References.MODID + ":textures/models/armor/JasperArmor_layer_1.png";

        }


        else if(stack.getItem() == FishMod.JasperLegs){
            return References.MODID + ":textures/models/armor/JasperArmor_layer_2.png";

        }

        else{
            return null;
        }

    }

}
