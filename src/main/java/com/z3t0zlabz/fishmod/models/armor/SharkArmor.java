package com.z3t0zlabz.fishmod.models.armor;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor.ArmorMaterial;

public class SharkArmor extends ItemArmor {
	 public SharkArmor(ArmorMaterial material, int id, int slot) {
		  super(material, id, slot);
		  this.setCreativeTab(FishMod.fishmod);
		  
		  //Icon Texture
		  	if (slot == 0) {
		  		this.setTextureName(References.MODID + ":Shark Helm"); //If Helmet
		  	}
		  	else if (slot == 1){
		  		this.setTextureName(References.MODID + ":Shark Chest");//If Chest
		  	}
		  	else if (slot == 2){
		  		this.setTextureName(References.MODID + ":Shark Legs"); //If Legs
		  	}
		  	else if (slot == 3){
		  		this.setTextureName(References.MODID + ":Shark Boots"); //If Boots
		  	}
		  
		 }
		 
		 @Override
		 public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
		 {

		  if(stack.getItem() == FishMod.SharkHelm || stack.getItem() == FishMod.SharkChest ||  stack.getItem() == FishMod.SharkBoots ){

		   return References.MODID + ":textures/models/armor/SharkArmor_layer_1.png";

		  }


		  else if(stack.getItem() == FishMod.SharkLegs){
		   return References.MODID + ":textures/models/armor/SharkArmor_layer_2.png";

		  }

		  else{
			  return null;
		  }
		  
		 }

}
