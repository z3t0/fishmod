package com.z3t0zlabz.fishmod.models.armor;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.lib.References;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class PyriteArmor extends ItemArmor {
    public PyriteArmor(ArmorMaterial material, int id, int slot) {
        super(material, id, slot);
        this.setCreativeTab(FishMod.fishmod);

        //Icon Texture
        if (slot == 0) {
            this.setTextureName(References.MODID + ":Pyrite Helm"); //If Helmet
        }
        else if (slot == 1){
            this.setTextureName(References.MODID + ":Pyrite Chest");//If Chest
        }
        else if (slot == 2){
            this.setTextureName(References.MODID + ":Pyrite Legs"); //If Legs
        }
        else if (slot == 3){
            this.setTextureName(References.MODID + ":Pyrite Boots"); //If Boots
        }

    }

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
    {

        if(stack.getItem() == FishMod.PyriteHelm || stack.getItem() == FishMod.PyriteChest ||  stack.getItem() == FishMod.PyriteBoots ){

            return References.MODID + ":textures/models/armor/PyriteArmor_layer_1.png";

        }


        else if(stack.getItem() == FishMod.PyriteLegs){
            return References.MODID + ":textures/models/armor/PyriteArmor_layer_2.png";

        }

        else{
            return null;
        }

    }

}
