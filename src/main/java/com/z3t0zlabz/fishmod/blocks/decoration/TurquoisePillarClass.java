package com.z3t0zlabz.fishmod.blocks.decoration;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

/**
 * Created by Z3T0Z on 23/07/2014.
 */
public class TurquoisePillarClass extends Block{
    @SideOnly(Side.CLIENT)
    private IIcon topBottom;
    @SideOnly(Side.CLIENT)
    private IIcon sides;
    public TurquoisePillarClass(Material rock) {
        super(rock);
        this.setCreativeTab(CreativeTabs.tabDecorations);
    }
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int p_149691_2_)
    {
        return side == 1 || side == 0 ? this.topBottom : (side == 2 || side == 3 || side == 4 || side == 5 ? this.sides : this.blockIcon);
    }
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_)
    {
        this.blockIcon = p_149651_1_.registerIcon("fishmod:Turquoise Pillar Side");
        this.topBottom = p_149651_1_.registerIcon("fishmod:Turquoise Pillar Top");
        this.sides = p_149651_1_.registerIcon("fishmod:Turquoise Pillar Side");
    }
}

