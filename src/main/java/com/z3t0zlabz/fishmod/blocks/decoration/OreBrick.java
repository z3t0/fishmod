package com.z3t0zlabz.fishmod.blocks.decoration;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;


/**
 * Created by Z3T0Z on 23/07/2014.
 */
public class OreBrick extends Block {
    public OreBrick(Material rock){
        super(rock);
        this.setStepSound(soundTypeStone);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setHardness(6);
        this.setResistance(60);
        this.setLightLevel(1);
    }
}
