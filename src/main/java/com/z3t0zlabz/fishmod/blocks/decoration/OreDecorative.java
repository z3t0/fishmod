package com.z3t0zlabz.fishmod.blocks.decoration;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Created by Z3T0Z on 23/07/2014.
 */
public class OreDecorative extends Block {
    public OreDecorative(Material rock) {
        super(rock);
        this.setStepSound(soundTypeStone);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setHardness(3);
        this.setResistance(25);
        this.setLightLevel(3);
    }
}
