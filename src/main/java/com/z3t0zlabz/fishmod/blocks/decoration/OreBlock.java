package com.z3t0zlabz.fishmod.blocks.decoration;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;


/**
 * Created by Z3T0Z on 23/07/2014.
 */
public class OreBlock extends Block {

    public OreBlock(Material rock) {
        super(rock);
        this.setStepSound(soundTypeSand);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setHardness(4);
        this.setResistance(30);
        this.setLightLevel(5);

    }
}


