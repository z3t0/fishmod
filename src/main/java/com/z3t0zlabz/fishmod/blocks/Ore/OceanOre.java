package com.z3t0zlabz.fishmod.blocks.Ore;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;


public class OceanOre extends Block {
	public OceanOre(Material rock) {
		super(rock);
		this.setStepSound(soundTypeStone);
        this.setHardness(3F);
		this.setResistance(10);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setLightLevel(50);		
		this.setHarvestLevel("pickaxe", 1);
	}


    }


