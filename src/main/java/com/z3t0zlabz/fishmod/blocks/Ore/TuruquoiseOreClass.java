package com.z3t0zlabz.fishmod.blocks.Ore;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Created by Z3T0Z on 25/07/2014.
 */
public class TuruquoiseOreClass extends Block {
    public TuruquoiseOreClass(Material rock) {
        super(rock);
        this.setStepSound(soundTypeStone);
        this.setHardness(1F);
        this.setResistance(10);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setLightLevel(50);
        this.setHarvestLevel("pickaxe", 2);
    }
}
