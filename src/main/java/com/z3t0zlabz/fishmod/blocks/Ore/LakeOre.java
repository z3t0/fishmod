package com.z3t0zlabz.fishmod.blocks.Ore;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class LakeOre extends Block{

	public LakeOre(Material rock) {
		super(rock);
		this.setStepSound(soundTypeStone);
		this.setResistance(10);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setLightLevel(50);
        this.setHardness(3F);
        this.setHarvestLevel("pickaxe",1);
	}
	
}
