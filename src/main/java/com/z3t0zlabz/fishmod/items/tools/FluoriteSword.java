package com.z3t0zlabz.fishmod.items.tools;

import com.z3t0zlabz.fishmod.lib.References;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemSword;
import net.minecraft.item.Item.ToolMaterial;

public class FluoriteSword extends ItemSword {

	public FluoriteSword(ToolMaterial tMaterial) {
		super(tMaterial);
		this.setMaxStackSize(1);
		this.setMaxDamage(200);
		//this.setCreativeTab(TABNAME HERE);
	}
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{this.itemIcon = iconRegister.registerIcon(References.MODID + ":" + this.getUnlocalizedName().substring(5));
	}

}
