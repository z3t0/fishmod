package com.z3t0zlabz.fishmod.items.tools;

import com.z3t0zlabz.fishmod.lib.References;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;

/**
 * Created by Z3T0Z on 22/07/2014.
 */
public class BerylHoe extends ItemHoe {
    public BerylHoe(Item.ToolMaterial tMaterial)
    {
        super(tMaterial);
        this.setMaxStackSize(1);
        this.setMaxDamage(200);
        //this.setCreativeTab(TABNAME HERE);
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister)
    {this.itemIcon = iconRegister.registerIcon(References.MODID + ":" + this.getUnlocalizedName().substring(5));
    }
}
