package com.z3t0zlabz.fishmod.items.food;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;


public class RawFood extends ItemFood {
    public RawFood(int i, int i1, boolean b) {
        super(i,i1,b);
        this.setCreativeTab(CreativeTabs.tabFood);
        this.setAlwaysEdible();
        this.setPotionEffect(Potion.moveSlowdown.id, 60, 2, 1F);

    }



}
