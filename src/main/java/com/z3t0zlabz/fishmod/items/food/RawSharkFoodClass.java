package com.z3t0zlabz.fishmod.items.food;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;
import net.minecraft.potion.Potion;

/**
 * Created by Z3T0Z on 22/07/2014.
 */
public class RawSharkFoodClass extends ItemFood {
    public RawSharkFoodClass(int i, int i1, boolean b) {
        super(i,i1,b);
        this.setCreativeTab(CreativeTabs.tabFood);
        this.setAlwaysEdible();
        this.setPotionEffect(Potion.damageBoost.id,60,2,1F);

    }
}
