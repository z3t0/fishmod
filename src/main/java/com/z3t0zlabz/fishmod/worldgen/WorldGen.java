//package com.z3t0zlabz.fishmod.worldgen;
//
//import java.util.Random;
//
//import net.minecraft.block.Block;
//import net.minecraft.world.World;
//import net.minecraft.world.chunk.IChunkProvider;
//import net.minecraft.world.gen.feature.WorldGenMinable;
//
//import com.z3t0zlabz.fishmod.FishMod;
//
//import cpw.mods.fml.common.IWorldGenerator;
//
//public class WorldGen implements IWorldGenerator{
//
//	
//	
//	@Override
//	public void generate(Random Par2Random,Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
//		
//		String s = par1World.getBiomeGenForCoords(chunkX + 8, chunkZ + 8).biomeName;
//		//Turquoise Ore in Ocean, yCoord == 34+
//		if (s.startsWith("Ocean")|| s.startsWith("River"))
//		{
//		int rarity = 1;
//		int veinSize = 50;
//		int height = 50;
//		for (int i = 0; i < rarity; ++i)
//		{
//			 int randomPosX = chunkX + par2Random.nextInt(16);
//			 int randomPosY = par2Random.nextInt(height);
//			 int randomPosZ = chunkZ + par2Random.nextInt(16);
//			 (new WorldGenMinable(FishMod.AmmoniteOre, veinSize)).generate(par1World, par2Random, randomPosX, randomPosY, randomPosZ);
//			 (new WorldGenMinable(FishMod.AquamarineOre, veinSize)).generate(par1World, par2Random, randomPosX, randomPosY, randomPosZ);
//			 
//		}
//		}
//
//			
//		case 0:
//		//generate our surface world
//		generateSurface(world, random, chunkX*16, chunkZ*16, chunkX, chunkZ);
//
//		case -1:
//			//generate our nether world
//			generateNether(world, random, chunkX*16, chunkZ*16);
//			
//		case 1:
//			//generate our end world
//			generateEnd(world, random, chunkX*16, chunkZ*16);
//		
//		}
//		
//	}
//
//	private void generateSurface(World world, Random random, int i, int j, int chunkX, int chunkZ) {
//
////	EXAMPLE	this.addOreSpawn(*OREBLOCK*, world, random, i, j, 16, 16, "VEIN SIZE(RANDOM IS RECOMMENDED)", 10, 38, 100);
//
//		this.addOreSpawn(chunkX, chunkZ, FishMod.AmmoniteOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		this.addOreSpawn(chunkX, chunkZ, FishMod.AquamarineOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		this.addOreSpawn(chunkX, chunkZ, FishMod.DiopsideOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		this.addOreSpawn(chunkX, chunkZ, FishMod.FluoriteOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		this.addOreSpawn(chunkX, chunkZ, FishMod.TrilobiteOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		this.addOreSpawn(chunkX, chunkZ, FishMod.TurquoiseOre, world, random, i, j, 16, 16, 4+random.nextInt(3), 4, 38, 100);
//		
//	}
//	
//	private void addOreSpawn(int chunkX, int chunkZ, Block ore, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chanceToSpawn, int minY, int maxY) {
//		String s = world.getBiomeGenForCoords(chunkX + 8, chunkZ + 8).biomeName;
//		//Turquoise Ore in Ocean, yCoord == 34+
//		if (s.startsWith("Ocean"))
//		{
//		System.out.println("ORE SPAWNED********************************************************************************************************");
//		for(int i = 0; i < chanceToSpawn; i++)
//		{int posX = blockXPos + random.nextInt(maxX);
//		int posY = minY + random.nextInt(maxY - minY);
//		int posZ = blockZPos + random.nextInt(maxZ);
//		(new WorldGenMinable(FishMod.TurquoiseOre, maxVeinSize)).generate(world, random, posX, posY, posZ);
//		
//		}
//		}
//		
//	}
//
//	private void generateNether(World world, Random random, int i, int j) {
//		
//		
//	}
//	
//	private void generateEnd(World world, Random random, int i, int j) {
//		
//		
//	}
//
//
//}