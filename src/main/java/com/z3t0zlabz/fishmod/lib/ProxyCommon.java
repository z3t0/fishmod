package com.z3t0zlabz.fishmod.lib;

//import com.z3t0zlabz.fishmod.blocks.Furnace.AmmoniteFurnaceClass;

import com.z3t0zlabz.fishmod.entity.entityclass.TunaFish;
import com.z3t0zlabz.fishmod.entity.model.ModelCod;
import com.z3t0zlabz.fishmod.entity.model.ModelTuna;
import com.z3t0zlabz.fishmod.entity.entityclass.CodFish;
import com.z3t0zlabz.fishmod.entity.render.RenderCod;
import com.z3t0zlabz.fishmod.entity.render.RenderTuna;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ProxyCommon {

    public void registerRenderThings() {
        //Entities
        RenderingRegistry.registerEntityRenderingHandler(TunaFish.class, new RenderTuna(new ModelTuna(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(CodFish.class, new RenderCod(new ModelCod(), 0.3F));
    }
}
