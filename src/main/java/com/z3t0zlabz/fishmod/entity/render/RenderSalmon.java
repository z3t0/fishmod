package com.z3t0zlabz.fishmod.entity.render;

import com.z3t0zlabz.fishmod.entity.entityclass.SalmonFish;
import com.z3t0zlabz.fishmod.entity.model.ModelSalmon;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

/**
 * Created by z3t0 on 30/07/14.
 */
public class RenderSalmon extends RenderLiving {

    private static final ResourceLocation texture = new ResourceLocation("fishmod:textures/entity/Salmon Texture.png");
    protected ModelSalmon modelEntity;
    public RenderSalmon(ModelBase par1ModelBase, float par2){
        super(par1ModelBase,par2);
        modelEntity = ((ModelSalmon) mainModel);
    }

    public void renderSalmon(SalmonFish entity, double x, double y, double z, float u, float v){
        super.doRender(entity,x ,y ,z ,u ,v );
    }

    public void doRenderLiving(EntityLiving entityLiving, double x, double y, double z, float u, float v){
        renderSalmon((SalmonFish)entityLiving,x ,y ,z ,u ,v );

    }

    public void doRender(Entity entity,double x, double y, double z, float u, float v){
        renderSalmon((SalmonFish)entity, x, y, z, u, v);

    }


    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return texture;
    }
}
