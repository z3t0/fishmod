package com.z3t0zlabz.fishmod.entity.render;

import com.z3t0zlabz.fishmod.FishMod;
import com.z3t0zlabz.fishmod.entity.entityclass.TunaFish;
import com.z3t0zlabz.fishmod.entity.model.ModelTuna;
import com.z3t0zlabz.fishmod.lib.References;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Z3T0Z on 26/07/2014.
 */
public class RenderTuna extends RenderLiving {

    private static final ResourceLocation texture = new ResourceLocation("fishmod:textures/entity/Tuna 0.3.png");
    protected ModelTuna modelEntity;
    public RenderTuna(ModelBase par1ModelBase, float par2){
        super(par1ModelBase,par2);
        modelEntity = ((ModelTuna) mainModel);
    }

    public void renderTuna(TunaFish entity, double x, double y, double z, float u, float v){
        super.doRender(entity,x ,y ,z ,u ,v );
    }

    public void doRenderLiving(EntityLiving entityLiving, double x, double y, double z, float u, float v){
       renderTuna((TunaFish)entityLiving,x ,y ,z ,u ,v );

    }

    public void doRender(Entity entity,double x, double y, double z, float u, float v){
        renderTuna((TunaFish)entity, x, y, z, u, v);

    }


    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return texture;
    }
}
