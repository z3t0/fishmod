package com.z3t0zlabz.fishmod.entity;

import com.z3t0zlabz.fishmod.FishMod;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.registry.EntityRegistry;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.biome.BiomeGenBase;

import java.util.Random;

/**
 * Created by Z3T0Z on 26/07/2014.
 */
public class EntityHandler {
    public static void registerEntities(Class entityClass,String name){
        int entityId = EntityRegistry.findGlobalUniqueEntityId();
        long x = name.hashCode();
        Random random = new Random(x);
        int mainColor = random.nextInt() * 16777215;
        int subColor = random.nextInt() * 16777215;

        EntityRegistry.registerGlobalEntityID(entityClass, name, entityId);
        EntityRegistry.addSpawn(entityClass,50,2,4, EnumCreatureType.waterCreature, BiomeGenBase.river, BiomeGenBase.ocean);
        EntityRegistry.registerModEntity(entityClass,name,entityId, FishMod.instance,64,1,true);
        EntityList.entityEggs.put(Integer.valueOf(entityId), new EntityList.EntityEggInfo(entityId, mainColor, subColor));

    }
}
