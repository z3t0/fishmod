package com.z3t0zlabz.fishmod.entity.entityclass;

import com.z3t0zlabz.fishmod.FishMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.passive.EntityWaterMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * Created by z3t0 on 29/07/14.
 */
public class CodFish extends EntityWaterMob {
    World world = null;

    public CodFish(World var1) {
        super(var1);
        world = var1;
        experienceValue = 5;
        this.isImmuneToFire = false;
        addRandomArmor();
        //this.tasks.addTask(1, new EntityAIWander(this, 0.8D)); //Allows movementn
        // this.tasks.addTask(2, new EntityAIAvoidEntity(this,EntityPlayer.class,5.0F,0.5D,0.5D));
        this.tasks.addTask(0, new EntityAILookIdle(this));


    }


    public boolean hasCustomNameTag() {
        return true;
    }


    protected void addRandomArmor() {

    }


    public boolean isAIEnabled() {
        return true;
    }

    /**
     * Drop 0-2 items of this living's type
     */
    protected void dropFewItems(boolean var1, int var2) {
        this.entityDropItem(new ItemStack(FishMod.RawCod), 0.0F);
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound() {
        return "";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound() {
        return "";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound() {
        return "";
    }

    public void onStruckByLightning(EntityLightningBolt entityLightningBolt) {
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;

    }

    protected void fall(float l) {
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;
        super.fall(l);

    }

    public void onCriticalHit(Entity entity) {
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;

    }

    public void onKillEntity(EntityLiving entityLiving) {
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;

    }

    public boolean interact(EntityPlayer entity) {
        int i = (int) this.posX;
        int j = (int) this.posY;
        int k = (int) this.posZ;

        return true;
    }

    public String getEntityName() {
        return "CodFish";
    }
}
