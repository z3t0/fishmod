package com.z3t0zlabz.fishmod.entity.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSalmon extends ModelBase {

    public ModelRenderer body;
    public ModelRenderer face;
    public ModelRenderer finBottomOne;
    public ModelRenderer finLeft;
    public ModelRenderer finRight;
    public ModelRenderer finTopOne;
    public ModelRenderer tailBaseOne;
    public ModelRenderer tailBaseTwo;
    public ModelRenderer tailOne;
    public ModelRenderer tailThree;
    public ModelRenderer tailTwo;

    public ModelSalmon() {

        textureWidth = 64;
        textureHeight = 32;

        body = new ModelRenderer(this, 13, 6);
        body.addBox(0F, 0F, 0F, 7, 7, 18);
        body.setRotationPoint(-4F, 3F, -12F);
        body.setTextureSize(128, 64);
        body.mirror = false;
        setRotation(body, 0F, 0F, 0F);

        face = new ModelRenderer(this, 47, 15);
        face.addBox(0F, 0F, 0F, 5, 5, 3);
        face.setRotationPoint(-3F, 4F, -15F);
        face.setTextureSize(128, 64);
        face.mirror = false;
        setRotation(face, 0F, 0F, 0F);

        finBottomOne = new ModelRenderer(this, 55, 9);
        finBottomOne.addBox(0F, 0F, 0F, 1, 2, 3);
        finBottomOne.setRotationPoint(-1F, 1F, 2F);
        finBottomOne.setTextureSize(128, 64);
        finBottomOne.mirror = false;
        setRotation(finBottomOne, 0F, 0F, 0F);

        finLeft = new ModelRenderer(this, 48, 10);
        finLeft.addBox(0F, 0F, 0F, 1, 2, 2);
        finLeft.setRotationPoint(3F, 4F, -8F);
        finLeft.setTextureSize(128, 64);
        finLeft.mirror = false;
        setRotation(finLeft, 0F, 0F, 0F);

        finRight = new ModelRenderer(this, 48, 5);
        finRight.addBox(0F, 0F, 0F, 1, 2, 2);
        finRight.setRotationPoint(-5F, 4F, -8F);
        finRight.setTextureSize(128, 64);
        finRight.mirror = false;
        setRotation(finRight, 0F, 0F, 0F);

        finTopOne = new ModelRenderer(this, 55, 2);
        finTopOne.addBox(0F, 0F, 0F, 1, 3, 3);
        finTopOne.rotateAngleX = 0.7853981633974483F;
        finTopOne.setRotationPoint(-1F, 8F, 0F);
        finTopOne.setTextureSize(128, 64);
        finTopOne.mirror = false;
        setRotation(finTopOne, 0F, 0F, 0F);

        tailBaseOne = new ModelRenderer(this, 12, 14);
        tailBaseOne.addBox(-3F, 4F, 6F, 5, 5, 4);
        tailBaseOne.setRotationPoint(-3F, 4F, 6F);
        tailBaseOne.setTextureSize(128, 64);
        tailBaseOne.mirror = false;
        setRotation(tailBaseOne, 0F, 0F, 0F);

        tailBaseTwo = new ModelRenderer(this, 13, 6);
        tailBaseTwo.addBox(0F, 0F, 0F, 3, 3, 4);
        tailBaseTwo.setRotationPoint(-2F, 5F, 10F);
        tailBaseTwo.setTextureSize(128, 64);
        tailBaseTwo.mirror = false;
        setRotation(tailBaseTwo, 0F, 0F, 0F);

        tailOne = new ModelRenderer(this, 24, 0);
        tailOne.addBox(0F, 0F, 0F, 1, 7, 2);
        tailOne.setRotationPoint(-1F, 3F, 11F);
        tailOne.setTextureSize(128, 64);
        tailOne.mirror = false;
        setRotation(tailOne, 0F, 0F, 0F);

        tailThree = new ModelRenderer(this, 3, 5);
        tailThree.addBox(0F, 0F, 0F, 1, 11, 3);
        tailThree.setRotationPoint(-1F, 1F, 15F);
        tailThree.setTextureSize(128, 64);
        tailThree.mirror = false;
        setRotation(tailThree, 0F, 0F, 0F);

        tailTwo = new ModelRenderer(this, 5, 20);
        tailTwo.addBox(0F, 0F, 0F, 1, 9, 2);
        tailTwo.setRotationPoint(-1F, 2F, 13F);
        tailTwo.setTextureSize(128, 64);
        tailTwo.mirror = false;
        setRotation(tailTwo, 0F, 0F, 0F);
    }
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        body.render(f5);
        face.render(f5);
        finBottomOne.render(f5);
        finLeft.render(f5);
        finRight.render(f5);
        finTopOne.render(f5);
        tailBaseOne.render(f5);
        tailBaseTwo.render(f5);
        tailOne.render(f5);
        tailThree.render(f5);
        tailTwo.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
    }
}