package com.z3t0zlabz.fishmod.entity.model;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelTuna extends ModelBase {

       //fields
    ModelRenderer body;
    ModelRenderer face;
    ModelRenderer finBottom;
    ModelRenderer finLeft;
    ModelRenderer finRight;
    ModelRenderer finTopOne;
    ModelRenderer tailBase;
    ModelRenderer tailFour;
    ModelRenderer tailOne;
    ModelRenderer tailThree;
    ModelRenderer tailTopThree;
    ModelRenderer tailTopTwo;
    ModelRenderer tailTwo;

    public ModelTuna() {
        textureWidth = 64;
        textureHeight = 32;

        body = new ModelRenderer(this, 1,9); // name and texture coordinates
        body.addBox(0F, 0F, 0F, 6, 6, 16); // (X,Y,Z, dimensions) OH I accidentally changed something
        body.setRotationPoint(0F, 16F, -8F);
        body.setTextureSize(textureWidth, textureHeight);
        body.mirror = true;
        setRotation(body, 0F, 0F, 0F);

        face = new ModelRenderer(this, 47,12);
        face.addBox(0F, 0F, 0F, 4, 4, 2);
        face.setRotationPoint(1F, 17F, -10F);
        face.setTextureSize(64,32);
        face.mirror = true;
        setRotation(face, 0F, 0F, 0F);

        finBottom = new ModelRenderer(this, 47,12);
        finBottom.addBox(0F, 0F, 0F, 1, 3, 2);
        finBottom.setRotationPoint(3F, 21F, -1F);
        finBottom.setTextureSize(64,32);
        finBottom.mirror = true;
        setRotation(finBottom, 0.5759586531581288F, 0F, 0F);

        finLeft = new ModelRenderer(this, 47,12);
        finLeft.addBox(0F, 0F, 0F, 1, 2, 7);
        finLeft.setRotationPoint(6F, 18F, -4F);
        finLeft.setTextureSize(64,32);
        finLeft.mirror = true;
        setRotation(finLeft, 0F, 0F, 0F);

        finRight = new ModelRenderer(this, 47,12);
        finRight.addBox(0F, 0F, 0F, 1, 2, 7);
        finRight.setRotationPoint(-1F, 18F, -4F);
        finRight.setTextureSize(64,32);
        finRight.mirror = true;
        setRotation(finRight, 0F, 0F, 0F);

        finTopOne = new ModelRenderer(this, 47,12);
        finTopOne.addBox(0F,0F,0F, 1, 1, 1);
        finTopOne.setRotationPoint(2F, 15F, -2F);
        finTopOne.setTextureSize(64,32);
        finTopOne.mirror = true;
        setRotation(finTopOne, 0F, 0F, 0F);

        tailBase = new ModelRenderer(this, 47,12);
        tailBase.addBox(0F, 0F, 0F, 1, 2, 2);
        tailBase.setRotationPoint(3F, 18F, 8F);
        tailBase.setTextureSize(64,32);
        tailBase.mirror = true;
        setRotation(tailBase, 0F, 0F, 0F);

        tailFour = new ModelRenderer(this,47,12);
        tailFour.addBox(0F, 0F, 0F, 1, 10, 1);
        tailFour.setRotationPoint(3F, 14F, 13F);
        tailFour.setTextureSize(128, 64);
        tailFour.mirror = true;
        setRotation(tailFour, 0F, 0F, 0F);

        tailOne = new ModelRenderer(this,47,12);
        tailOne.addBox(0F, 0F, 0F, 1, 4, 1);
        tailOne.setRotationPoint(3F, 17F, 10F);
        tailOne.setTextureSize(64,32);
        tailOne.mirror = true;
        setRotation(tailOne, 0F, 0F, 0F);

        tailThree = new ModelRenderer(this,47,12);
        tailThree.addBox(0F, 0F, 0F, 1, 8, 1);
        tailThree.setRotationPoint(3F, 15F, 12F);
        tailThree.setTextureSize(64,32);;
        tailThree.mirror = true;
        setRotation(tailThree, 0F, 0F, 0F);

        tailTopThree = new ModelRenderer(this, 47,12);
        tailTopThree.addBox(0F, 0F, 0F, 1, 3, 1);
        tailTopThree.setRotationPoint(2F, 15F, 0F);
        tailTopThree.setTextureSize(64,32);
        tailTopThree.mirror = true;
        setRotation(tailTopThree, 0F, 0F, 0F);

        tailTopTwo = new ModelRenderer(this, 47,12);
        tailTopTwo.addBox(0F, 0F, 0F, 1, 2, 1);
        tailTopTwo.setTextureSize(64,32);
        tailTopTwo.setRotationPoint(2F, 15F, -1F);
        tailTopTwo.mirror = true;
        setRotation(tailTopTwo, 0F, 0F, 0F);

        tailTwo = new ModelRenderer(this,47,12);
        tailTwo.addBox(0F, 0F, 0F, 1, 6, 1);
        tailTwo.setTextureSize(64,32);
        tailTwo.setRotationPoint(3F, 16F, 11F);
        tailTwo.mirror = true;
        setRotation(tailTwo, 0F, 0F, 0F);

    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        body.render(f5);
        face.render(f5);
        finBottom.render(f5);
        finLeft.render(f5);
        finRight.render(f5);
        finTopOne.render(f5);
        tailBase.render(f5);
        tailFour.render(f5);
        tailOne.render(f5);
        tailThree.render(f5);
        tailTopThree.render(f5);
        tailTopTwo.render(f5);
        tailTwo.render(f5);

    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }

}