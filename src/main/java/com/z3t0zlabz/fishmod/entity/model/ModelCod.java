package com.z3t0zlabz.fishmod.entity.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelCod extends ModelBase {

    //fields
    public ModelRenderer body;
    public ModelRenderer bodyTwo;
    public ModelRenderer face;
    public ModelRenderer finLeft;
    public ModelRenderer finRight;
    public ModelRenderer finTopOne;
    public ModelRenderer tailBaseOne;
    public ModelRenderer tailBaseTwo;
    public ModelRenderer tailBottomOne;
    public ModelRenderer tailBottomTwo;
    public ModelRenderer tailFour;
    public ModelRenderer tailOne;
    public ModelRenderer tailThree;
    public ModelRenderer tailTopThree;
    public ModelRenderer tailTopTwo;
    public ModelRenderer tailTwo;

    public ModelCod() {
        textureWidth = 64;
        textureHeight = 32;

        body = new ModelRenderer(this, 19, 11);
        body.addBox(0F, 0F, 0F, 7, 5, 15);
        body.setRotationPoint(0F, 19F, -9F);
        body.setTextureSize(128, 64);
        body.mirror = false;
        setRotation(body, 0F, 0F, 0F);

        bodyTwo = new ModelRenderer(this, 17, 19);
        bodyTwo.addBox(0F, 0F, 0F, 5, 3, 3);
        bodyTwo.setRotationPoint(1F, 20F, 6F);
        bodyTwo.setTextureSize(128, 64);
        bodyTwo.mirror = false;
        setRotation(bodyTwo, 0F, 0F, 0F);

        face = new ModelRenderer(this, 19, 7);
        face.addBox(0F, 0F, 0F, 5, 3, 2);
        face.setRotationPoint(1F, 20F, -11F);
        face.setTextureSize(128, 64);
        face.mirror = false;
        setRotation(face, 0F, 0F, 0F);

        finLeft = new ModelRenderer(this, 43, 6);
        finLeft.addBox(0F,0F,0F, 1, 1, 3);
        finLeft.setRotationPoint(7F, 21F, -6F);
        finLeft.setTextureSize(128, 64);
        finLeft.mirror = false;
        setRotation(finLeft, 0F, 0F, 0F);

        finRight = new ModelRenderer(this, 26, 2);
        finRight.addBox(0F, 0F, 0F, 1, 1, 3);
        finRight.setRotationPoint(-1F, 21F, -6F);
        finRight.setTextureSize(128, 64);
        finRight.mirror = false;
        setRotation(finRight, 0F, 0F, 0F);

        finTopOne = new ModelRenderer(this, 59, 3);
        finTopOne.addBox(0F, 0F, 0F, 1, 2, 1);
        finTopOne.setRotationPoint(3F, 18F, -5F);
        finTopOne.setTextureSize(128, 64);
        finTopOne.mirror = false;
        setRotation(finTopOne, 0F, 0F, 0F);

        tailBaseOne = new ModelRenderer(this, 17, 14);
        tailBaseOne.addBox(0F, 0F, 0F, 5, 1, 3);
        tailBaseOne.setRotationPoint(1F, 21F, 9F);
        tailBaseOne.setTextureSize(128, 64);
        tailBaseOne.mirror = false;
        setRotation(tailBaseOne, 0F, 0F, 0F);

        tailBaseTwo = new ModelRenderer(this, 51, 21);
        tailBaseTwo.addBox(0F, 0F, 0F, 3, 1, 3);
        tailBaseTwo.setRotationPoint(2F, 21F, 12F);
        tailBaseTwo.setTextureSize(128, 64);
        tailBaseTwo.mirror = false;
        setRotation(tailBaseTwo, 0F, 0F, 0F);

        tailBottomOne = new ModelRenderer(this, 19, 3);
        tailBottomOne.addBox(0F, 0F, 0F, 1, 1, 2);
        tailBottomOne.setRotationPoint(3F, 18F, -5F);
        tailBottomOne.setTextureSize(128, 64);
        tailBottomOne.mirror = false;
        setRotation(tailBottomOne, 0F, 0F, 0F);

        tailBottomTwo = new ModelRenderer(this, 33, 3);
        tailBottomTwo.addBox(0F, 0F, 0F, 1, 1, 4);
        tailBottomTwo.setRotationPoint(3F, 18F, 1F);
        tailBottomTwo.setTextureSize(128, 64);
        tailBottomTwo.mirror = false;
        setRotation(tailBottomTwo, 0F, 0F, 0F);

        tailFour = new ModelRenderer(this, 12, 14);
        tailFour.addBox(0F, 0F, 0F, 1, 7, 1);
        tailFour.setRotationPoint(3F, 18F, 15F);
        tailFour.setTextureSize(128, 64);
        tailFour.mirror = false;
        setRotation(tailFour, 0F, 0F, 0F);

        tailOne = new ModelRenderer(this, 13, 2);
        tailOne.addBox(0F, 0F, 0F, 1, 3, 1);
        tailOne.setRotationPoint(3F, 20F, 12F);
        tailOne.setTextureSize(128, 64);
        tailOne.mirror = false;
        setRotation(tailOne, 0F, 0F, 0F);

        tailThree = new ModelRenderer(this, 12, 23);
        tailThree.addBox(0F, 0F, 0F, 1, 7, 1);
        tailThree.setRotationPoint(3F, 18F, 14F);
        tailThree.setTextureSize(128, 64);
        tailThree.mirror = false;
        setRotation(tailThree, 0F, 0F, 0F);

        tailTopThree = new ModelRenderer(this, 52, 5);
        tailTopThree.addBox(0F, 0F, 0F, 1, 3, 4);
        tailTopThree.setRotationPoint(3F, 18F, 1F);
        tailTopThree.setTextureSize(128, 64);
        tailTopThree.mirror = false;
        setRotation(tailTopThree, 0F, 0F, 0F);

        tailTopTwo = new ModelRenderer(this, 52, 13);
        tailTopTwo.addBox(0F, 0F, 0F, 1, 3, 4);
        tailTopTwo.setRotationPoint(3F, 18F, -4F);
        tailTopTwo.setTextureSize(128, 64);
        tailTopTwo.mirror = false;
        setRotation(tailTopTwo, 0F, 0F, 0F);

        tailTwo = new ModelRenderer(this, 13, 7);
        tailTwo.addBox(0F, 0F, 0F, 1, 5, 1);
        tailTwo.setRotationPoint(3F, 19F, 13F);
        tailTwo.setTextureSize(128, 64);
        tailTwo.mirror = false;
        setRotation(tailTwo, 0F, 0F, 0F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        body.render(f5);
        bodyTwo.render(f5);
        face.render(f5);
        finLeft.render(f5);
        finRight.render(f5);
        finTopOne.render(f5);
        tailBaseOne.render(f5);
        tailBaseTwo.render(f5);
        tailBottomOne.render(f5);
        tailBottomTwo.render(f5);
        tailFour.render(f5);
        tailOne.render(f5);
        tailThree.render(f5);
        tailTopThree.render(f5);
        tailTopTwo.render(f5);
        tailTwo.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}